import React from 'react';
import ReactDom from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client';
import { ApolloProvider } from '@apollo/client/react';

import { ConstProvider } from './context/Context';

import './css/imports.css';


import App from './components/App';


const link = createHttpLink({
    uri: 'https://development.inov-8.com/graphql',
    headers: {
      'Access-Control-Allow-Origin': '*',
     }
  });


  const client = new ApolloClient({
    cache: new InMemoryCache(),
    link
  });


ReactDom.render(
    <ApolloProvider client={client}>
        <BrowserRouter>
        <ConstProvider>
            <App />
            </ConstProvider>
        </BrowserRouter>
    </ApolloProvider>, document.querySelector('#react_root'));