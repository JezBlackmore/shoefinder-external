import React from 'react';

const Context = React.createContext();

export function ConstProvider({children}){

    const [showSideNav, setShowSideNav] = React.useState(false);
    const [activeProduct, setActiveProduct] = React.useState([]);
    const [activityType, setActivityType] = React.useState('');
    const [showList, setShowList] = React.useState(false);
    const [showDontHavSize, setShowDontHavSize] = React.useState(false);
    const [showSizeGuide, setShowSizeGuide] = React.useState(false);

    return(
        <Context.Provider value={{
            showSideNav,
            setShowSideNav,
            activeProduct,
            setActiveProduct,
            activityType,
            setActivityType,
            showList,
            setShowList,
            showDontHavSize,
            setShowDontHavSize,
            showSizeGuide,
            setShowSizeGuide
        }}>
        {children}
        </Context.Provider>
      )
}


export default Context;




