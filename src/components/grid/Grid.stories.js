import React from 'react';
import Grid from './Grid';

export default {
    title: 'Grid Systems/Grids',
    component: Grid
}

export const TwoColmns = () => {
    return(
        <Grid columns={2}>
               <p>Column 1</p>
            <p>Column 2</p>
            <p>Column 3</p>
            <p>Column 4</p>
            <p>Column 5</p>
            <p>Column 6</p>
            <p>Column 7</p>
            <p>Column 8</p>
            <p>Column 9</p>
            <p>Column 10</p>
        </Grid>
    )
}

export const ThreeColmns = () => {
    return(
        <Grid columns={3}>
              <p>Column 1</p>
            <p>Column 2</p>
            <p>Column 3</p>
            <p>Column 4</p>
            <p>Column 5</p>
            <p>Column 6</p>
            <p>Column 7</p>
            <p>Column 8</p>
            <p>Column 9</p>
            <p>Column 10</p>
        </Grid>
    )
}

export const FourColmns = () => {
    return(
        <Grid columns={4}>
             <p>Column 1</p>
            <p>Column 2</p>
            <p>Column 3</p>
            <p>Column 4</p>
            <p>Column 5</p>
            <p>Column 6</p>
            <p>Column 7</p>
            <p>Column 8</p>
            <p>Column 9</p>
            <p>Column 10</p>
        </Grid>
    )
}


export const FiveColmns = () => {
    return(
        <Grid columns={5}>
            <p>Column 1</p>
            <p>Column 2</p>
            <p>Column 3</p>
            <p>Column 4</p>
            <p>Column 5</p>
            <p>Column 6</p>
            <p>Column 7</p>
            <p>Column 8</p>
            <p>Column 9</p>
            <p>Column 10</p>
        </Grid>
    )
}
