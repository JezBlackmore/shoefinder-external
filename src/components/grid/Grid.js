import React from 'react';
import './Grid.css';

const Grid = ({ children, columns }) => {
    return (
        <div className={columns === 1 ? "gridx1" :
            columns === 2 ? "gridx2" :
            columns === 3 ? "gridx3" :
            columns === 4 ? "gridx4":
            "gridx5"}>
            {children}
        </div>
    )
}

export default Grid;