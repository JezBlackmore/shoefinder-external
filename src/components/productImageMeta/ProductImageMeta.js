import React from 'react';
import './ProductImageMeta.css';

const ProductImageMeta = ({ product, thumbnailMainImage }) => {
    return (
        <>
            {/* <div className="salesSlug"><span>{product.saleSlug}</span></div> */}
            <img src={thumbnailMainImage/* product.image.url */} alt={product.image.label}/>
        </>
    )
}

export default ProductImageMeta;