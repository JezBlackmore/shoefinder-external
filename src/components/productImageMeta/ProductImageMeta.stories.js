import React from 'react';

import ProductImageMeta from './ProductImageMeta';
import Grid from '../grid/Grid';



const product = {
    "image": {
        "label": "inov-8 Fastlift 335 Women's - rear",
        "url": "https://www.inov-8.com/media/catalog/product/cache/bd1b8cad7c61ae1ddc34981e467e2b22/0/0/000050-bkbk.jpg"
      },
}


export default {
    title: 'Shoe Finder/Product Meta Combined/Individual/Product Image Meta',
    component: ProductImageMeta,
    argTypes: {
        product: { control: 'object' }
    }
}


const Template = (args) => <Grid columns={3}><ProductImageMeta {...args} /></Grid>

export const productImageMeta  = Template.bind({})
productImageMeta.args = {
    product: product
}

/* 
const product = [
    {
        "id": 1,
        "title": "X-Talon 235 Men's",
        "slug": "x-talon-235-Mens",
        "price": 140.00,
        "rating": 4,
        "gender": "male",
        "oppGenderId": 6,
        "color": ["Black/Orange"],
        "sizes": [
            {"size": 7, "status": false}, 
            {"size": 7.5, "status": false},
            {"size": 8, "status": false},
            {"size": 8.5, "status": false},
            {"size": 9, "status": false},
            {"size": 9.5, "status": false},
            {"size": 10, "status": false},
            {"size": 10.5, "status": false},
            {"size": 11, "status": false},
            {"size": 11.5, "status": false},
            {"size": 12, "status": false},
            {"size": 12.5, "status": false},
            {"size": 13, "status": false}
        ],
        "saleSlug": "5 ColourWays",
        "description": "Super-grippy, lightweight shoe featuring revolutionary graphene-grip outsoles",
        "features": ["Revolutionary graphene-enhanced G-GRIP outsole", "Light and fast", "Super Durable"],
        "photos": ["https://www.inov-8.com/media/catalog/product/cache/111a8af1e99ed4b2d5fcd19443eb9ee7/0/0/000910-ORBK-P-01-X-Talon-G-235-M-Orange-Black-1.jpg"],
        "stats": {
                "SnowIce": 2,
                "softMuddy": 8,
                "fit": 4,
                "cushioning": 6,
                "hardRocky": 4,
                "pathsTrail": 8,
                "responsiveness": 10,
                "breathability": 6,
                "road": 2
        },
        "filterStatus": {
                "fells": 100,
                "mountains": 20,
                "trails": 80,
                "ultra": 60,
                "ocr": 50,
                "orienteering": 50,
                "parkRun": 70,
                "road": 100,
                "swimrun": 30,
                "hardAndRocky": 50,
                "softAndMuddy": 40,
                "roads": 100,
                "pathAndTrails": 70,
                "iceAndSnow": 77,
                "daily": 100,
                "training": 40,
                "competition": 20,
                "narrow": 100,
                "normal": 87,
                "wide": 12,
                "soft": 34,
                "moderate": 43,
                "firm": 17
        },
        "category": "shoe",
        "shipping": "Express"
    }
] */