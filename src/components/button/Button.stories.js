import React, { Component } from 'react';
import Button from './Button';

export default {
    title: 'Buttons/Button',
    component: Button,
    argTypes: {
        style: {control: {
            type: 'radio',
            options: ['primarySF', 'secondarySF']
        }},
        link: {control: 'text'},
        text: {control: 'text'},
        onSelectButton: { action: 'clicked' }
    }
}

const Template = (args) => <Button {...args} />

export const Primary = Template.bind({})
Primary.args = {
    style: "primarySF",
    link: "https://www.inov-8.com/",
    text: 'FIND OUT MORE'
}

export const Secondary = Template.bind({})
Secondary.args = {
    style: "secondarySF",
    link: "https://www.inov-8.com/",
    text: 'FIND OUT MORE'
}