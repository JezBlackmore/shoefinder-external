import React from 'react';
import './Button.css';

const Button = ({ link, text, style, onSelectButton }) => {
    return(
        <button href={link} onClick={onSelectButton} className={`btnSF ${style}`}>{text}</button>
    )
}

export default Button;