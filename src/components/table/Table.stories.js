import React from 'react';

import Grid from '../grid/Grid';

import Table from './Table';


const cells = {
    lines: [
        ['UK size', 'EU size', 'US mens', 'US womens', 'Sizes in CM (Approx.)'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
    ]
}

export default {
    title: 'Tables/Table',
    component: Table,
    argTypes: {
        tableHeaderFontSize: {control: 'number'},
        tableHeaderFontColor: {control: 'color'},
        tableBodyFontSize: {control: 'number'},
        tableHeaderBackGroundColor: {control: 'color'},
        defaultColour: {control: 'color'},
        textAlign: {
            control: {
            type: 'radio',
            options: ['left', 'center']
            }
        },
        cells: cells,
    }
}


const Template = (args) => <Grid columns={2}><Table {...args} /></Grid>

export const table = Template.bind({})
table.args = {
    tableHeaderFontSize: 10,
    tableHeaderFontColor: "white",
    tableBodyFontSize: 10,
    tableHeaderBackGroundColor: "black",
    defaultColour: 'black',
    textAlign: 'center',
    cells: cells,
}
