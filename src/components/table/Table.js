import React from 'react';

import './Table.css';

const Table = ({ 
    cells, 
    tableHeaderFontSize,
    tableBodyFontSize,
    tableHeaderBackGroundColor,
    defaultColour,
    textAlign,
    tableHeaderFontColor }) => {

/*    EXAMPLE:  const test = {
        lines: [
            ['', 'Men', 'Women'],
            ['ESSENTIAL FATS', '2-5%', '10-13%'],
            ['ATHLETES', '6-13%', '14-20%'],
            ['FITNESS', '14-17%', '21-24%'],
            ['AVERAGE', '18-24%', '25-31%'],
            ['OBESE', '25%+', '32%+']
        ]
    } */


    return(

        <div className="table" style={{ color: defaultColour }}>
            
            {cells.lines.map((lines, i) => {
                return (
                <div className="tableRow">
                    {lines.map((cell, index) => {
                         return i === 0? <div className="tableColumn" style={{backgroundColor: tableHeaderBackGroundColor, borderColor: defaultColour}}><p style={{fontSize: tableHeaderFontSize, color: tableHeaderFontColor}}>{cell}</p></div> :
                            <div className="tableColumn" style={{ borderColor: defaultColour }}><p style={{fontSize: tableBodyFontSize, textAlign: textAlign}}>{cell}</p></div>
                        })
                    }
                </div>)
            })}
        </div>

    )
}

export default Table;