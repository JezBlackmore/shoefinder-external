import React from 'react';
import './Filter.css';

import FilterButton from './FilterButton4';

const Options = ({ 
    option,
    chosenChoices,
    setShowReset,
    setSelectOnFilterButtonPress,
    setSelectOnInformationButton,
    disabledButtons }) => {

    const [showInfo, setShowInfo] = React.useState(false);

    return(
        <>
        <div className="titleOption">
         <h4>{option.subTitle} { option.subTitle && option.labelInformation.data.length > 0 && <i className="fa fa-info-circle" aria-hidden="true" onClick={() => setSelectOnInformationButton(option)}></i>}</h4> 
                {showInfo && <div className="titleOption__label">
                    <p>{option.labelInformation}</p>
                </div>}
        </div>
        <div className="buttonArea">
            {option.buttons.map( (activity, i) => {
                return <FilterButton 
                    activity={activity} 
                    key={`activity${i}`} 
                    chosenChoices={chosenChoices}
                    setShowReset={setShowReset}
                    setSelectOnFilterButtonPress={setSelectOnFilterButtonPress}    
                    disabledButtons={disabledButtons}
                />
            })}
        </div>
        </>
    )
}

export default Options;