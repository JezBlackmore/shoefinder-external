import React, { useState, useEffect } from 'react';
import './Filter.css';

import ButtonsToShow from './ButtonsToShow2';

const FilterProducts = ({  
    chosenChoices,
    setChosenChoices,
    type,
    buttons,
    setSelectOnFilterButtonPress,
    setSelectBackButton,
    setSelectOnFilterReset,
    setSelectOnInformationButton,
    disabledButtons}) => {
    
    const [buttonsToShow, setButtonsToShow ] = React.useState('');
    const [showReset, setShowReset ] = React.useState(false);

    
  React.useEffect(() => {
        for(const buttonType in buttons) {
          
            if(type === buttons[buttonType].type){
                setButtonsToShow([buttons[buttonType]]);
            }

        }
    }, [chosenChoices, showReset, type, buttons])  
 


    const resetChoice = () => {
        setChosenChoices({});
        setShowReset(false);
    }

return (
    buttonsToShow ? <div className="filterProducts">
           
                 <ButtonsToShow 
                    selectedButtonGroup={buttons.find(group => group.type === type)}
                    chosenChoices={chosenChoices}
                   
                    setShowReset={setShowReset}
                    setSelectOnFilterButtonPress={setSelectOnFilterButtonPress}
                    setSelectBackButton={setSelectBackButton}
                    setSelectOnInformationButton={setSelectOnInformationButton}
                    disabledButtons={disabledButtons}
                  
                />
           

        <div className="resetEverything">{ showReset && <button className="secondaryFilter" onClick={() => {
            setSelectOnFilterReset()
            resetChoice()
        }
        }>Start Over</button>}
        
        </div>
    </div>: <p>Loading...</p>
)       
    }   

export default FilterProducts;