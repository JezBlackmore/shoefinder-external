import React from 'react';
import './Filter.css';

const FilterButton = ({ activity, chosenChoices, setShowReset, setSelectOnFilterButtonPress, disabledButtons }) => {

    /*  console.log(6666666, activity.identifier, disabledButtons[activity.identifier]); */
   

     const [pressed, setPressed] = React.useState(false);
     const [disable, setDisable] = React.useState(false);

    const clicker = (e) => {
   /*   setDisable(disabledButtons[activity.identifier]) */
     if(chosenChoices){
        setShowReset(true);
     } 
     
     setPressed(!pressed);
      /*   const splitValue = e.target.value.split('-');   */

     
          if(chosenChoices[e.target.value]){
               chosenChoices[e.target.value] = e.target.value;
          } else{
               chosenChoices[e.target.value] = e.target.value;
          }
     
       setSelectOnFilterButtonPress(chosenChoices, activity, pressed);
      
    }

React.useEffect(() => {
     if(disabledButtons[activity.identifier] === undefined){
          setDisable(false)
     } else {
          setDisable(!disabledButtons[activity.identifier])
     }
}, [disabledButtons, pressed, activity])




/*  return   (
      <>
     {disable && <button disabled={disable}  className={`btn ${pressed ? 'selected' : 'secondaryFilter'}`} value={activity.value} onClick={(e) => clicker(e)}>{activity.name}</button>}

     {!disable &&  <button  className={`btn ${pressed ? 'selected' : 'secondaryFilter'}`} value={activity.value} onClick={(e) => clicker(e)}>{activity.name}</button>}
     </>
 ) */
/* } else {
     return   <button  className={`btn ${pressed ? 'selected' : 'secondaryFilter'}`} value={activity.value} onClick={(e) => clicker(e)}>{activity.name}</button>;
} */

 if(disable){
     return <button disabled={true} className={`btnSF disabled`} value={activity.value} onClick={(e) => clicker(e)}>{activity.name}</button> 
 } else{
     return   <button  className={`btnSF ${pressed ? 'selected' : 'secondaryFilter'}`} value={activity.value} onClick={(e) => clicker(e)}>{activity.name}</button>;
 }
        /*   return <button disabled={disabledButtons[activity.indentifer]} className={`btn disabled`} value={activity.value} onClick={(e) => clicker(e)}>{activity.name}</button> */
     /* }  else{
          return   <button  className={`btn ${pressed ? 'selected' : 'secondaryFilter'}`} value={activity.value} onClick={(e) => clicker(e)}>{activity.name}</button>;
     } */
             
}

export default FilterButton;

