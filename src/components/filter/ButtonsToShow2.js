import React from 'react';

import Options from './Options3';

const ButtonsToShow = ({ 
    selectedButtonGroup,
    chosenChoices,
    setShowReset,
    setSelectBackButton,
    setSelectOnFilterButtonPress,
    setSelectOnInformationButton,
    disabledButtons }) => {
    return(
        <>
        <h2>{selectedButtonGroup.activityTitle}</h2>
        <p className="filterProducts__back" onClick={setSelectBackButton}><i className="fa fa-long-arrow-left" aria-hidden="true"></i><span>BACK A STEP</span></p>
        {selectedButtonGroup.options.map((option, i) => {
            return <Options 
                option={option} key={i}
                chosenChoices={chosenChoices}
                setShowReset={setShowReset}
                setSelectOnFilterButtonPress={setSelectOnFilterButtonPress}
                setSelectOnInformationButton={setSelectOnInformationButton}
                disabledButtons={disabledButtons}
            />
        })}
        </>
    )
}

export default ButtonsToShow;