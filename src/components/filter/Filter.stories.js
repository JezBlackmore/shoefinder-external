import React, { useState } from 'react';

import Filter from './Filter';


import Grid from '../grid/Grid';




const buttons = [{
    type: "running",
    activityTitle: "Your Activity - Running",
    options: [{
                subTitle: "TYPE OF RUN ACTIVITY?",
                labelInformation: "Info",
                buttons: [{name: "Fells", status: false, value: 'activity-901'}, {name:"Mountains", status: false, value: 'activity-45'}, {name:"Trails", status: false, value: 'activity-897'}, {name:"Ultra", status: false, value: 'activity-898'}, {name:"OCR", status: false, value: 'activity-902'},  {name:"Orienteering", status: false, value: 'activity-903'}, {name:"Park Run", status: false, value: 'activity-914'}, {name:"Road", status: false, value: 'activity-899'}, {name:"SwimRun", status: false, value: 'activity-915'} ]
                },
                {
                subTitle: "TYPE OF TERRAIN?",
                labelInformation: "Info",
                buttons: [{name:"Hard & Rocky", status: false, value: 'grip-grip_hard_rocky'}, {name:"Soft & Muddy", status: false, value: 'grip-grip_soft_muddy'}, {name:"Roads", status: false, value: 'grip-grip_road'}, {name:"Path & Trails", status: false, value: 'grip-grip_paths_trails'},{name:"Ice & Snow", status: false, value: 'grip-grip_snow_ice'}]
                },
                {
                subTitle: "WHAT WIDTH DO YOU PREFER?",
                labelInformation: "Info",
                buttons: [{name:"Narrow", status: false, value: 'fit_filter-1'}, {name:"Normal", status: false, value: 'fit_filter-3'}, {name:"Wide", status: false, value: 'fit_filter-5'}]
                },
                {
                subTitle: "WHAT TYPE OF CUSHIONING?",
                labelInformation: "Info",
                buttons: [{name:"soft", status: false, value: 'shoe_attribute_cushioning-1'}, {name:"moderate", status: false, value: 'shoe_attribute_cushioning-2'}, {name:"firm", status: false, value: 'shoe_attribute_cushioning-3'}]
                }]
},

{
    type: "hiking",
    activityTitle: "Your Activity - Hiking",
    options: [{
                subTitle: "WHAT TYPE OF FOOTWEAR?",
                labelInformation: "Info",
                buttons: [{name: "Boot", status: false, value: 'product_feed_type-boot'}, {name:"Shoe", status: false, value: 'product_feed_type-shoe'}]
                },
                {
                subTitle: "PREFERRED WEIGHT?",
                labelInformation: "Info",
                buttons: [{name:"<250g", status: false, value: 'product_weight-<250'}, {name:"250g - 300g", status: false, value: 'product_weight-200_300'}, {name:">350g", status: false, value: 'product_weight->350'}]
                },
                {
                subTitle: "WHAT WIDTH DO YOU PREFER?",
                labelInformation: "Info",
                buttons: [{name:"Narrow", status: false, value: 'fit_filter-1'}, {name:"Normal", status: false, value: 'fit_filter-3'}, {name:"Wide", status: false, value: 'fit_filter-5'}]
                },
                {
                subTitle: "PREFERRED HEEL DROP?",
                labelInformation: "Info",
                buttons: [{name:"6mm", status: false, value: 'drop-6'}, {name:"8mm", status: false, value: 'drop-8'}, {name:"9mm", status: false, value: 'drop-9'}]
                }]
},
{
    type: "gym",
    activityTitle: "Your Activity - Gym",
    options: [{
                subTitle: "WHAT’S YOUR SESSION?",
                labelInformation: "Info",
                buttons: [{name: "Workout", status: false, value: 'activity-893'}, {name:"WeightLifting", status: false, value: 'activity-895'}, {name:"Run", status: false, value: 'activity-894'}, {name:"HIIT", status: false, value: 'hiit'}]
                },
                {
                subTitle: "WHAT WIDTH DO YOU PREFER?",
                labelInformation: "Info",
                buttons: [{name:"Narrow", status: false, value: 'fit_filter-1'}, {name:"Normal", status: false, value: 'fit_filter-3'}, {name:"Wide", status: false, value: 'fit_filter-5'}]
                },
                {
                subTitle: "PRIMARY COLOUR?",
                labelInformation: "Info",
                buttons: [{name:"Red", status: false, value: 'label-red'}, {name:"Blue", status: false, value: 'label-blue'}, {name:"Green", status: false, value: 'label-green'}, {name:"White", status: false, value: 'label-white'}]
                }]
}]


/* const chosenChoices = {
    activity: '',
    grip: '',
    fit_filter: '',
    shoe_attribute_cushioning: '',
    product_feed_type: '',
    product_weight: '',
    drop: ''
} */
const chosenChoicesObj = {};


export default {
    title: 'Shoe Finder/Filter',
    component: Filter,
    argTypes: {
        type: {control: {
            type: 'radio',
            options: ['running', 'hiking', 'gym']
        }},
        buttons: buttons
    }
}



const Template = (args) => {
    const [chosenChoices , setChosenChoices] = useState(chosenChoicesObj)
    return(
       <Grid columns={2}><Filter {...args} chosenChoices={chosenChoices} setChosenChoices={setChosenChoices}/></Grid>
    )
}

export const filter = Template.bind({})
filter.args = {
    type: 'running',
    buttons: buttons
}

