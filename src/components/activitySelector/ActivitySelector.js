import React, { useEffect, useState } from 'react';
import './ActivitySelector.css';

const ActivitySelector = ({ 
    setSelectOnActivityTypeButton, 
    ActivitySelectorOptions, 
    activeProduct, 
    setActiveProduct,
    backToOverviewButton}) => {

    /* console.log(7777, activeProduct.length) */
    /* const [activitiesToShow, setActivitiesToShow] = useState(["running", "gym", "hiking"]) */
const [showBackButton, setShowBackButton] = useState(false)

/* useEffect(() => {
    
    if(activeProduct.length < 0){
        setShowBackButton(false)
    } else {
        setShowBackButton(true)
    }


}, []) */

    return(
        <div className="ActivitySelector">
            <div className="ActivitySelector__introText">
                <h2>FIND YOUR PERFECT SHOE</h2>
                <h3>SELECT YOUR ACTIVITY BELOW</h3>
                <div className="width75">
                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo</p>
                </div>
                <h4>ACTIVITY</h4>
            </div>
            <div className="ActivitySelector__buttons">
                {ActivitySelectorOptions.map((activity , i) => {
                    return <button onClick={() => setSelectOnActivityTypeButton(activity)} key={`activity${i}`}>{activity}</button>
                })}
            </div>
            {activeProduct.length > 0 ? <div className="resetEverything">
                 <button className="secondaryFilterBack" onClick={() => backToOverviewButton()}>BACK TO OVERVIEW</button>
            </div>: ''}
        </div>
    )
}

export default ActivitySelector; 