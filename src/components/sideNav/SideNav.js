import React from 'react';
import './SideNav.css';

const SideNav = ({ showSideNav, setShowSideNav, children, topAndBottomPadding , sidePadding  }) => {

    const clickBackground = (e) => {
        setShowSideNav(true);
    }


    return (
        showSideNav && 
        <div className="sideNav__outer" onClick={(e) => clickBackground(e)}>
            <div className="sideNav" style={{padding: `${topAndBottomPadding}px ${sidePadding}px ${topAndBottomPadding}px ${sidePadding}px`}} onClick={(e) =>  e.stopPropagation()}>
                <button onClick={() => setShowSideNav(false)} className="closeSideNav">Close</button>
                { children }
            </div>
        </div>
    )
}

export default SideNav;