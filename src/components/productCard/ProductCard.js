import React from 'react';
import './ProductCard.css';

const ProductCard = ({ product, onSelectProductCard }) => {

/*     const { setChosenProduct,
        showProduct, 
        setShowProduct } = useContext(Context);*/

    const [stars, setStars] = React.useState([])
  /*    const cardClicked = () => {
        window.scrollTo(0, 0);
        setChosenProduct(product);
        setShowProduct(true);
    }

    useEffect(() => {
        const numberOfStars = () => {
           const array = new Array(product.rating).fill(1);
           while(array.length < 5){
             array.push(0);
           }
           setStars(array)
        }
        numberOfStars();
    }, [product]); */

 /*    console.log(product) */
    return (
        <>
       
         <div className={`productCard ${product.hasOwnProperty('knockedOut') && product.knockedOut === true ? `knockedOut`: ``}`} onClick={() => onSelectProductCard(product)}>
            <div className="productCard__image">
                <img src={product.thumbnail.url} alt={product.thumbnail.label}/>
            </div>
            <div className="productCard__title">
                <h4>{product.displayName}</h4>
            </div>
           {/*  <div className="productCard__rating">
            <p>{stars.map((star, i) => star? <i key={`star${i}`} className="fa fa-star" aria-hidden="true"></i>: <i key={`star${i}`} className="fa fa-star-o" aria-hidden="true"></i>)}</p>
            </div> */}
          {/*   <div className="productCard__description">
            <p>{product.description}</p>
            </div>
            <div className="productCard__color">
                <p>{product.color.length} colour</p>
            </div> */}

            
            <div className="productCard__price">
              {
          
          product.price.regularPrice ?  <p className="amount"><strong>{`${currency(product.price.regularPrice.amount.currency)}${product.price.regularPrice.amount.value.toFixed(2)}` }</strong></p>:
              <p>No Price Listed</p> } 
            </div>
        </div>
        </>
    )
}

export default ProductCard;

const currency = (value) => {
    let newValue = `${value}`;
    switch (newValue) {
        case "GBP":
            newValue = "£";
            break;
        case "USD":
            newValue = "$";
            break;
        case "EUR":
            newValue = "€";
            break;
    }
    return newValue;
} 
