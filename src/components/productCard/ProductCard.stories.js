import React from 'react';
import ProductCard from './ProductCard';

import Grid from '../grid/Grid';


const product = {
    "name": "All Terrain Pro Mitt",
    "regularPrice": {
        "adjustments": [],
        "amount": {
          "value": 140,
          "currency": "GBP"
        }
    },
    "special_price": null,
    "thumbnail": {
        "label": "inov-8 All Terrain Pro Mitt - sideview",
        "url": "https://www.inov-8.com/media/catalog/product/cache/bd1b8cad7c61ae1ddc34981e467e2b22/a/l/all_terrain_pro_mitt.jpg"
    }
}


export default {
    title: 'Shoe Finder/Product Card',
    component: ProductCard,
    argTypes: {
        product: { control: 'object' }
    }
}

const Template = (args) => <Grid columns={4}><ProductCard {...args} /></Grid>

export const productCard = Template.bind({})
productCard.args = {
    product: product
}






