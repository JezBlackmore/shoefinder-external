import React, { useState } from 'react';

import SideNavLeft from './SideNavLeft';
import DontHaveYourSizeForm from '../dontHaveYourSizeForm/DontHaveYourSizeForm'
import Table from '../table/Table';


export default {
    title: 'Shoe Finder/Side Navigation',
    component: SideNavLeft,
    argTypes: {
        sidePadding: { control: 'number' },
        topAndBottomPadding: { control: 'number' }
    }
}


const Template1 = (args) => {
    const [showSideNav, setShowSideNav] = useState(false);
    return (
        <>
            <button onClick={() => setShowSideNav(!showSideNav)}>Toggle Side Nav</button>
            {showSideNav && <SideNav showSideNav={showSideNav} setShowSideNav={setShowSideNav} {...args}>
                <h3>Don't have your size?</h3>
                <p>Select your size below and we’ll email you when it’s back in stock</p>
                <DontHaveYourSizeForm 
                 textColour={'black'}
                 acceptBGcolor={'white'}
                 acceptFontColor={'black'}
                 gender={'male'}
                 tableColor={'white'}
                 linkColor={'white'}
                />
            </SideNav>}
        </>
    )
}

export const dontHaveSize = Template1.bind({})
dontHaveSize.args = {
    sidePadding: 40,
    topAndBottomPadding: 40
}



const cells = {
    lines: [
        ['UK size', 'EU size', 'US mens', 'US womens', 'Sizes in CM (Approx.)'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
    ]
}

const Template2 = (args) => {
    const [showSideNav, setShowSideNav] = useState(false);
    return (
        <>
            <button onClick={() => setShowSideNav(!showSideNav)}>Toggle Side Nav</button>
            {showSideNav && <SideNav showSideNav={showSideNav} setShowSideNav={setShowSideNav} {...args}>
                <>
                    <h3>Size Guide</h3>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
                    <Table 
                        cells={cells}
                        tableHeaderFontSize={10}
                        tableBodyFontSize={10}
                        tableHeaderBackGroundColor={"white"}
                        defaultColour={"white"}
                        textAlign={"center"}
                        tableHeaderFontColor={"black"}
                    />
                </>
            </SideNav>}
        </>
    )
}

export const sizeGuide = Template2.bind({})
sizeGuide.args = {
    sidePadding: 40,
    topAndBottomPadding: 40
}


const Template3 = (args) => {
    const [showSideNav, setShowSideNav] = useState(false);
    return (
        <>
            <button onClick={() => setShowSideNav(!showSideNav)}>Toggle Side Nav</button>
            {
                showSideNav && <SideNav showSideNav={showSideNav} setShowSideNav={setShowSideNav} {...args}>
                    <>
                        <h3>SOFT</h3>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
                        <h3>MODERATE</h3>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
                        <h3>FIRM</h3>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
                    </>
                </SideNav>
            }
        </>
    )
}

export const informationList = Template3.bind({})
informationList.args = {
    sidePadding: 40,
    topAndBottomPadding: 40
}

