import React from 'react';
import './SideNavLeft.css';

const SideNavLeft = ({ showSideNavLeft, setShowSideNavLeft, children, topAndBottomPadding , sidePadding  }) => {

    const clickBackground = (e) => {
        setShowSideNavLeft(false)
    }

    return (
        showSideNavLeft && 
        <div className="sideNavLeft__outer" onClick={(e) => clickBackground(e)}>
        <div className="sideNavLeft" style={{padding: `${topAndBottomPadding}px ${sidePadding}px ${topAndBottomPadding}px ${sidePadding}px`}} onClick={(e) =>  e.stopPropagation()}>
            <button onClick={() => setShowSideNavLeft(false)} className="closeSideNavLeft">Close</button>
            { children }
        </div>
        </div>
    )
}

export default SideNavLeft;