import React from 'react';

import './RelatedProducts.css';

import RecommendCard from './RecommendCard';



const ProductPrefer = ({ 
    related, 
    onSelectRelatedProducts,
    setThumbnailMainImage }) => {

/*     const { products } = useContext(Context);

    const [recommnendArray, setRecommendArray] = useState(products.slice(0, 3)); */

    return (
        <div className="prefer">
            <h3>You Might Prefer</h3>

            <div className="recommendations">
                {related.map((product, i) => {
                    return <RecommendCard 
                    product={product} 
                    key={`recommendation${i}`} 
                    onSelectRelatedProducts={onSelectRelatedProducts}
                    setThumbnailMainImage={setThumbnailMainImage}/>
                })}
            </div>
        </div>
    )
}

export default ProductPrefer;