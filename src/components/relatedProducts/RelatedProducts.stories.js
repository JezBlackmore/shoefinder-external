import React from 'react';
import RelatedProducts from './RelatedProducts';

import Grid from '../grid/Grid';




const related = [{
        "name": "All Terrain Pro Mitt",
        "regularPrice": {
            "adjustments": [],
            "amount": {
                "value": 140,
                "currency": "GBP"
            }
        },
        "special_price": null,
        "thumbnail": {
            "label": "inov-8 All Terrain Pro Mitt - sideview",
            "url": "https://www.inov-8.com/media/catalog/product/cache/bd1b8cad7c61ae1ddc34981e467e2b22/a/l/all_terrain_pro_mitt.jpg"
        }
    },
    {
        "name": "All Terrain Pro Mitt",
        "regularPrice": {
            "adjustments": [],
            "amount": {
                "value": 140,
                "currency": "GBP"
            }
        },
        "special_price": null,
        "thumbnail": {
            "label": "inov-8 All Terrain Pro Mitt - sideview",
            "url": "https://www.inov-8.com/media/catalog/product/cache/bd1b8cad7c61ae1ddc34981e467e2b22/a/l/all_terrain_pro_mitt.jpg"
        }
    },
    {
        "name": "All Terrain Pro Mitt",
        "regularPrice": {
            "adjustments": [],
            "amount": {
                "value": 140,
                "currency": "GBP"
            }
        },
        "special_price": null,
        "thumbnail": {
            "label": "inov-8 All Terrain Pro Mitt - sideview",
            "url": "https://www.inov-8.com/media/catalog/product/cache/bd1b8cad7c61ae1ddc34981e467e2b22/a/l/all_terrain_pro_mitt.jpg"
        }
    }
]

export default {
    title: 'Shoe Finder/Related Products',
    component: RelatedProducts,
    argTypes: {
        related:  {control: 'object' }
    }
}

const Template = (args) => <Grid columns={1}><RelatedProducts {...args} /></Grid>

export const relatedProduct = Template.bind({})
relatedProduct.args = {
    related: related
}