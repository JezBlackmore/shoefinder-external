  
import React from 'react';

import './RelatedProducts.css';

/* import Context from '../contexts/Context'; */

const RecommendCard = ({product, onSelectRelatedProducts}) => {

 /*    const { setChosenProduct,
        showProduct, 
        setShowProduct } = useContext(Context); */

 /*    const cardClicked = () => {
        window.scrollTo(0, 0);
        setChosenProduct(product);
        setShowProduct(true);
    } */
    return (
        <div className="recommendCard" onClick={() => onSelectRelatedProducts(product)}>
            <div className="recommendCard__image">
                <img src={product.thumbnail.url} alt={product.thumbnail.label}/>
            </div>
            <div className="recommendCard__title">
                <h4>{product.name}</h4>
            </div>
        </div>
    )
}

export default RecommendCard;