import React from 'react';
/* import { Link } from 'react-router-dom'; */
import ChoiceCard from './ChoiceCard';


const SelectionCard = ({ cards, onSelectSectionCard, linkSectionCard }) => {

 
    return (
      <div className="finder__container">
            {cards.map((card, i) => <ChoiceCard card={card} key={`card${i}`} onSelectSectionCard={onSelectSectionCard} linkSectionCard={linkSectionCard}/>)}
      </div>
    )
}

export default SelectionCard;