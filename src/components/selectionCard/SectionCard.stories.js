import React from 'react';
import SelectionCard from './SelectionCard';

import Grid from '../grid/Grid';


const cards = [
    {
        background: "https://www.inov-8.com/media/catalog/product/cache/bd1b8cad7c61ae1ddc34981e467e2b22/a/l/all_terrain_pro_mitt.jpg",
        image: "https://www.inov-8.com/media/catalog/product/cache/bd1b8cad7c61ae1ddc34981e467e2b22/a/l/all_terrain_pro_mitt.jpg",
        text: "Run",
        link: "https://www.inov-8.com/"
    },
    {
        background: "",
        image: "https://www.inov-8.com/media/catalog/product/cache/bd1b8cad7c61ae1ddc34981e467e2b22/a/l/all_terrain_pro_mitt.jpg",
        text: "Gym",
        link: "https://www.inov-8.com/"
    },
    {
        background: '',
        image: "https://www.inov-8.com/media/catalog/product/cache/bd1b8cad7c61ae1ddc34981e467e2b22/a/l/all_terrain_pro_mitt.jpg",
        text: "Hike",
        link: "https://www.inov-8.com/"
    }
]
export default {
    title: 'Shoe Finder/SelectionCard',
    component: SelectionCard,
    argTypes: {
        cards: {control: 'object'},
        linkSectionCard: {control: 'text'},
        onSelectSectionCard: { action: 'clicked' }
    }
}

const Template = (args) =>  <SelectionCard {...args} />


export const selectionCard = Template.bind({})
selectionCard.args = {
    cards: cards,
    linkSectionCard: '/filter',
}