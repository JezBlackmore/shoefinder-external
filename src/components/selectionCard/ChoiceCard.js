import React from 'react';
import { Link } from 'react-router-dom'; 

import './SelectionCard.css';


const ChoiceCard = ({ card, onSelectSectionCard, linkSectionCard }) => {
    return (
        <Link to={linkSectionCard }>
        <div className="finder__choice__card" style={{ backgroundImage: `url(${card.background})`}} onClick={() => onSelectSectionCard(card.text)}>
          <div className="finder__choice__card__container"> 
            <div className="finder__choice__card__container__center">
                    <img src={card.image} alt={card.text} />
                    <div className="finder__choice__card__float">
                        <div className="finder__choice__card__float__banner">
                            <h4>{card.text}</h4>
                        </div>
                    </div>
                </div>
           </div> 
        </div>
        </Link>
    )
}

export default ChoiceCard;