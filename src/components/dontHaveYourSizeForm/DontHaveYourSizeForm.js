import React, { useState, useEffect } from 'react';

import './DontHaveYourSizeForm.css';

import SizePicker from '../sizePicker/SizePicker';

const DontHaveYourSizeForm = ({ textColour, acceptBGcolor, gender, tableColor, acceptFontColor, linkColor }) => {

    const [emailAddress, setEmailAddress] = useState('');

    const [sizes, setSizes] = useState('');

    const submittedForm = (e) => {
        e.preventDefault();
        setEmailAddress('');
        alert("The form has been submitted");
    }

    useEffect(() => {
        if(gender === 'male'){
            setSizes({
                sizes: [6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10, 10.5, 11, 11.5, 12, 13, 14],
                pressed: false
            })
        } else if(gender === 'female'){
            setSizes({
                sizes: [3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5],
                pressed: false
            })
        }
    }, [gender]);


    return (
        <div className="dontHaveYourSizeForm">
            <form onSubmit={(e) => submittedForm(e)}>
                {sizes && <SizePicker gender={gender} product={sizes} tableColor={tableColor}/>}
                <input  className="dontHaveYourSizeForm__email" value={emailAddress} required style={{ borderColor: textColour, color: textColour}} placeholder="Email Address *" onChange={(e) => setEmailAddress(e.target.value)}/>
                <a href="https://www.inov-8.com" style={{ color: linkColor}}>View our privacy policy</a>
                <div style={{ backgroundColor: acceptBGcolor }} className="agreeContainer">
                    <input className="dontHaveYourSizeForm__agree" type="radio" id="acceptToReceive" name="agree" value="agree" />
                    <p style={{color: acceptFontColor}}>I agree to receiving messages about Inov-8 products, events, offers and discounts from Inov-8</p>
                </div>
                <button className="btnSF primarySF" type="submit">Add To Basket</button>
            </form>
        </div>
    )
}

export default DontHaveYourSizeForm;


