import React from 'react';
import DontHaveYourSizeForm from './DontHaveYourSizeForm';

import Grid from '../grid/Grid';



export default {
    title: 'Forms/Email Sign Up Form',
    component: DontHaveYourSizeForm,
    argTypes: {
        textColour: { control: 'color' },
        acceptBGcolor: { control: 'color' },
        acceptFontColor: { control: 'color' },
        gender: {
            control: {
              type: 'radio',
              options: ['male', 'female']
            }
          },
        tableColor: { control: 'color' },
        linkColor: { control: 'color' }
    }
}

const Template = (args) => <Grid columns={4}><DontHaveYourSizeForm {...args} /></Grid>

export const dontHaveSizeForm = Template.bind({})
dontHaveSizeForm.args = {
    textColour: 'black',
    acceptBGcolor: 'white',
    acceptFontColor: 'black',
    gender: 'male',
    tableColor: 'black',
    linkColor: 'black'
}