import React from 'react';
import ProductMetaCombined from './ProductMetaCombined';

import Grid from '../grid/Grid';




/* 
const product = {
    "name": "X-Talon 235 Men's",
    "price": 200,
    "sku": 2323,
    "thumbnail": "https://www.inov-8.com/media/catalog/product/cache/111a8af1e99ed4b2d5fcd19443eb9ee7/0/0/000910-ORBK-P-01-X-Talon-G-235-M-Orange-Black-1.jpg",
    "h2": "SUPERIOR STABILITY & SUPPORT WITH NATURAL FLEXIBILTY - THE ULTIMATE VERSATILE TRAINING SHOE",
    "short_description": "<ul><li>Superior heel support</li><li>Flexible & breathable upper</li><li>Lightweight stability & protection</li></ul>",
    "product_care": "<a href=`{{store direct_url='product-care'}}`>Product Care Instructions</a>",
    "status": true,
    "main_category": {
        "Running": false,
        "Training/Gym": false,
        "Hiking": false,
        "Clothing/Equipment": false
    },
    "genders": {
        "Men": false,
        "Women": false,
        "Unisex": false,
        "Kids": false
    },
    "configurations": [
        {
            "status": true,
            "colours": ["red", "green", "blue"],
            "shoe_size": 8
        },
        {
            "status": true,
            "colours": ["red", "green", "blue"],
            "shoe_size": 9
        },
        {
            "status": true,
            "colours": ["red", "green", "blue"],
            "shoe_size": 10
        },
        {
            "status": true,
            "colours": ["red", "green", "blue"],
            "shoe_size": 11
        },
        {
            "status": true,
            "colours": ["red", "green", "blue"],
            "shoe_size": 12
        },
        {
            "status": true,
            "colours": ["red", "green", "blue"],
            "shoe_size": 13
        }
    ],
    "fit_filter": 3,
    "shoe_attribute_cushioning": 2,
    "shoe_attribute_fit": 2,
    "shoe_attribute_underfoot_protection": 3,
    "shoe_attribute_responsiveness": 4,
    "shoe_attribute_breathability": 3,
    "main_category": {
        "Running": false,
        "Training/Gym": false,
        "Hiking": false,
        "Clothing/Equipment": false
    },
    "activity": {
        "Workout": false,
        "Running": false,
        "Weight Lifting": false,
        "Strength Training": false,
        "Trail Running": false,
        "Ultra Running": false,
        "Road Running": false,
        "Hiking": false,
        "Fell Running": false,
        "OCR": false,
        "Orienteering": false,
        "Parkrun": false,
        "SwimRun": false
    },
    "grip": {
        "grip_snow_ice": 2,
        "grip_soft_muddy": 3,
        "grip_hard_rocky": 2,
        "grip_paths_trails": 3,
        "grip_road": 4,
        "grip_versatility": 2,
        "grip_lifting": 4,
        "grip_cardio": 3,
        "grip_natural": 3
    },
    "Related Products": [
        {
            "ID": 23,
            "Thumbnail": "url",
            "Name": "test",
            "Status": true,
            "Attribute Set": "",
            "SKU": "",
            "Price": 150
        },
        {
            "ID": 23,
            "Thumbnail": "url",
            "Name": "test",
            "Status": true,
            "Attribute Set": "",
            "SKU": "",
            "Price": 150
        }
    ]
} */




const product = {
    "name": "Fastlift 335 Women's",
    "regularPrice": {
        "adjustments": [],
        "amount": {
          "value": 140,
          "currency": "GBP"
        }
    },
    "short_description": {
        "html": "<ul><li>Lightweight</li><li>Superior flexibility and comfort</li><li>Added stability for workouts with lifting</li></ul>"
    },
    "image": {
        "label": "inov-8 Fastlift 335 Women's - rear",
        "url": "https://www.inov-8.com/media/catalog/product/cache/bd1b8cad7c61ae1ddc34981e467e2b22/0/0/000050-bkbk.jpg"
    },
    "grip": {
        "grip_snow_ice": 2,
        "grip_soft_muddy": 3,
        "grip_hard_rocky": 2,
        "grip_paths_trails": 3,
        "grip_road": 4,
        "grip_versatility": 2,
        "grip_lifting": 4,
        "grip_cardio": 3,
        "grip_natural": 3
    },
    "configurable_options": [
        {
          "id": 5597,
          "label": "Shoe Size (UK)",
          "product_id": 9876,
          "attribute_code": "shoe_size",
          "values": [
            {
              "value_index": 45,
              "label": "3",
              "store_label": "3",
              "default_label": "3",
              "use_default_value": true
            },
            {
              "value_index": 46,
              "label": "3.5",
              "store_label": "3.5",
              "default_label": "3.5",
              "use_default_value": true
            },
            {
              "value_index": 47,
              "label": "4",
              "store_label": "4",
              "default_label": "4",
              "use_default_value": true
            },
            {
              "value_index": 48,
              "label": "4.5",
              "store_label": "4.5",
              "default_label": "4.5",
              "use_default_value": true
            },
            {
              "value_index": 49,
              "label": "5",
              "store_label": "5",
              "default_label": "5",
              "use_default_value": true
            },
            {
              "value_index": 50,
              "label": "5.5",
              "store_label": "5.5",
              "default_label": "5.5",
              "use_default_value": true
            },
            {
              "value_index": 15,
              "label": "6",
              "store_label": "6",
              "default_label": "6",
              "use_default_value": true
            },
            {
              "value_index": 16,
              "label": "6.5",
              "store_label": "6.5",
              "default_label": "6.5",
              "use_default_value": true
            },
            {
              "value_index": 17,
              "label": "7",
              "store_label": "7",
              "default_label": "7",
              "use_default_value": true
            },
            {
              "value_index": 18,
              "label": "7.5",
              "store_label": "7.5",
              "default_label": "7.5",
              "use_default_value": true
            },
            {
              "value_index": 51,
              "label": "8",
              "store_label": "8",
              "default_label": "8",
              "use_default_value": true
            },
            {
              "value_index": 52,
              "label": "8.5",
              "store_label": "8.5",
              "default_label": "8.5",
              "use_default_value": true
            }
          ]
        },
        {
          "id": 5852,
          "label": "Colours",
          "product_id": 9876,
          "attribute_code": "colours",
          "values": [
            {
              "value_index": 636,
              "label": "Black/Grey",
              "store_label": "Black/Grey",
              "default_label": "Black/Grey",
              "use_default_value": true
            },
            {
              "value_index": 808,
              "label": "Grey/Purple",
              "store_label": "Grey/Purple",
              "default_label": "Grey/Purple",
              "use_default_value": true
            },
            {
              "value_index": 818,
              "label": "Light Blue/Gum",
              "store_label": "Light Blue/Gum",
              "default_label": "Light Blue/Gum",
              "use_default_value": true
            },
            {
              "value_index": 843,
              "label": "Teal/White",
              "store_label": "Teal/White",
              "default_label": "Teal/White",
              "use_default_value": true
            }
          ]
        }
      ]

}

export default {
    title: 'Shoe Finder/Product Meta Combined',
    component: ProductMetaCombined,
    argTypes: {
        product: { control: 'object' }
    }
}

const Template = (args) => <ProductMetaCombined {...args} />

export const productMetaCombined  = Template.bind({})
productMetaCombined .args = {
    product: product
}


