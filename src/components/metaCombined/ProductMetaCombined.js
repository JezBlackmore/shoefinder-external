import React from 'react';

import './ProductMetaCombined.css';
import GenderPicker from '../genderPicker/GenderPicker'
import ProductImageMeta from '../productImageMeta/ProductImageMeta';
import ProductIntroMeta from '../productIntroMeta/ProductIntroMeta';
import SizePicker from '../sizePicker/SizePicker';
import Button from '../button/Button';
import ProductIntroMetaStatistics from '../productIntroMeta/ProductIntroMetaStatistics';
import ProductIntroMetaTitle from '../productIntroMeta/ProductIntroMetaTitle';
import GenderPickerMobile from '../genderPicker/GenderPickerMobile';

const ProductMetaCombined = ({ 
    product,
    onSelectSizeGuide,
    onSelectDontHaveSize,
    onSelectFindOutMore,
    onSelectSizePicker,
    genderOptions,
    onSelectAddToBasket,
    showAddToBasket,
    onSelectGenderPicker,
    setSelectedColor,
    selectedColor,
    chosenGenderMorF,
    genderPickerFirstLoad,
    thumbnailMainImage
 }) => {

    console.log(55555, product)
    return(
        <div className="productMeta">
            <div className="productImageMeta">
        <ProductImageMeta 
            product={product[0]}
            thumbnailMainImage={thumbnailMainImage}/>
        </div>
       
      
            <div className="productInfo__title">
                <ProductIntroMetaTitle 
                    product={product[0]}
                    selectedColor={selectedColor}/>
            </div>
            <div className="productInfo__stats">
                <ProductIntroMetaStatistics product={product[0]}/>
            </div>
       
      {/*   <ProductIntroMeta product={product[0]}/> */}


     {/*    <div className="productMeta__size"> */}
            <div div className="sizePicker">
            <SizePicker 
                product={product[0]}
                onSelectSizeGuide={onSelectSizeGuide}
                onSelectDontHaveSize={onSelectDontHaveSize}
                onSelectSizePicker={onSelectSizePicker}
                onSelectFindOutMore={onSelectFindOutMore}/>
            
           
           <div className="productButtons">
           {showAddToBasket ?  <Button style={"primarySF"} text={'Add To Basket'} onSelectButton={() => onSelectAddToBasket(product)}/>: <Button style={"disabledSF"} text={'Select Size'} onSelectButton={() => onSelectAddToBasket(product)}/>}
           <Button style={"secondarySF"} text={'FIND OUT MORE'} onSelectButton={() => onSelectFindOutMore(product)}/>
           </div>
           </div>
     {/*    </div> */}
        
        <div className="genderPicker">
            <GenderPicker 
                product={product} 
                onSelectGenderPicker={onSelectGenderPicker}
                setSelectedColor={setSelectedColor}
                selectedColor={selectedColor}
                chosenGenderMorF={chosenGenderMorF}
                genderPickerFirstLoad={genderPickerFirstLoad}
            />
        </div>

        <div className="genderPickerMobile">
            <GenderPickerMobile 
                product={product} 
                onSelectGenderPicker={onSelectGenderPicker}
                setSelectedColor={setSelectedColor}
                selectedColor={selectedColor}
                chosenGenderMorF={chosenGenderMorF}
                genderPickerFirstLoad={genderPickerFirstLoad}
             />
        </div>
    </div>
    )
}

export default ProductMetaCombined;