import React, { useEffect, useState } from 'react';

import './ProductIntroMeta.css';


const ProductIntroMetaTitle = ({ product, selectedColor }) => {

    const [stars, setStars] = useState([])


   /*  const [index, setIndex ] = useState(null); */
   const [coloursToDisplay, setColoursToDisplay] = useState(null) ;

   useEffect(() => {
    product.configurable_options[0].attribute_code === "colours" ? setColoursToDisplay('0') : setColoursToDisplay(1);


   /*  console.log(333333,product.configurable_options[0].values[1].colour.default_label) */
   }) 
    
  

 

    return (
       
          <>
                <h3>{product.name}</h3>
                <p className="productInfo__price"><strong>{`${currency(product.price.regularPrice.amount.currency)}${product.price.regularPrice.amount.value.toFixed(2)}`}</strong></p>
                {/*   <p  className="productInfo__stars">{stars.map((star, i) => star? <i key={`star${i}`} className="fa fa-star" aria-hidden="true"></i>: <i key={`star${i}`} className="fa fa-star-o" aria-hidden="true"></i>)}</p> */}
                <div className="productInfo__colours">
                    <strong>Colour: </strong>
                    { selectedColor ? 
                    <span >{`${selectedColor } `}</span> :
                    coloursToDisplay ? <span >{`${product.configurable_options[coloursToDisplay].values[0].default_label} `}</span>
                    : <span></span>}
                </div>
            </>
      
      
    )
}

export default ProductIntroMetaTitle;


const currency = (value) => {
    let newValue = `${value}`;
    switch (newValue) {
        case "GBP":
            newValue = "£";
            break;
        case "USD":
            newValue = "$";
            break;
        case "EUR":
            newValue = "€";
            break;
    }
    return newValue;
} 
