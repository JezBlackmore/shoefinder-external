import React, { useEffect, useState } from 'react';

import './ProductIntroMeta.css';


const ProductIntroMetaStatistics = ({ product }) => {
/* 
    const [stars, setStars] = useState([])


    const findIndex = () => product.configurable_options[0].attribute_code === "colours" ? 0 : 1;
    
    const [coloursToDisplay, setColoursToDisplay] = useState(findIndex)
 */

    const [grip, setGrip] = useState(null)
 
useEffect(() => {

    setGrip({
        "grip_hard_rocky": product.grip_hard_rocky,
        "grip_soft_muddy":  product.grip_soft_muddy,
        "grip_road": product.grip_road,
        "grip_paths_trails": product.grip_paths_trails,
        "grip_snow_ice": product.grip_snow_ice,
    })

}, [product])

    

    return (
       
        grip && <>
                <ul className="productInfo__bullets">

                    <div dangerouslySetInnerHTML={{__html: product.short_description.html }}></div>
                </ul>
                <div className="productInfo__stat">
                    {Object.keys(grip).map((attribute, i) => {
                        return (<div key={`stat${i}`}>
                            <h5>{getLabel(attribute)}</h5>
                            <div className="productInfo__stat__bar">
                                <div className="productInfo__stat__bar__percent" style={{ width: `${(grip[attribute] * 10) * 2}%`, backgroundColor: '#00ff00', height: "4px" }}></div>
                            </div>
                        </div>)
                    })
                    } 
                </div>
            </>
    )
}

export default ProductIntroMetaStatistics;


const getLabel = (label) => {
    let newlabel = `${label}`;
    switch (newlabel) {
        case "grip_snow_ice":
            newlabel = "Snow & Ice";
            break;
        case "grip_soft_muddy":
            newlabel = "Soft & Muddy";
            break;
        case "grip_hard_rocky":
            newlabel = "Hard & Rocky";
            break;
        case "grip_paths_trails":
            newlabel = "Paths & Trails";
            break;
        case "grip_road":
            newlabel = "Road";
            break;
        case "grip_versatility":
            newlabel = "Versatility";
            break;
        case "grip_lifting":
            newlabel = "Lifting";
            break;
        case "grip_cardio":
            newlabel = "Cardio";
            break;
        case "grip_natural":
            newlabel = "Natural";
            break;
    }
    return newlabel;
}

