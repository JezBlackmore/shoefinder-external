import React, { useEffect, useState } from 'react';

import './ProductIntroMeta.css';


const ProductIntroMeta = ({ product }) => {

    const [stars, setStars] = useState([])


    const findIndex = () => product.configurable_options[0].attribute_code === "colours" ? 0 : 1;
    
    const [coloursToDisplay, setColoursToDisplay] = useState(findIndex)

 

    return (
        <div className="productInfo">
            <div className="productInfo__title">
                <h3>{product.name}</h3>
                <p className="productInfo__price"><strong>{`${currency(product.price.regularPrice.amount.currency)}${product.price.regularPrice.amount.value.toFixed(2)}`}</strong></p>
                {/*   <p  className="productInfo__stars">{stars.map((star, i) => star? <i key={`star${i}`} className="fa fa-star" aria-hidden="true"></i>: <i key={`star${i}`} className="fa fa-star-o" aria-hidden="true"></i>)}</p> */}
                <div className="productInfo__colours">
                    <strong>Colour: </strong>
                    {product.configurable_options[coloursToDisplay].values.map((colour, i) => {
                        return  <span key={`color${i}`}>{`${colour.default_label} `}</span>
                    })}
                </div>
            </div>
            <div className="productInfo__stats">
                <ul className="productInfo__bullets">

                    <div dangerouslySetInnerHTML={{__html: product.short_description.html }}></div>
                </ul>
                <div className="productInfo__stat">
                    {/* {Object.keys(product.grip).map((attribute, i) => {
                        return (<div key={`stat${i}`}>
                            <h5>{getLabel(attribute)}</h5>
                            <div className="productInfo__stat__bar">
                                <div className="productInfo__stat__bar__percent" style={{ width: `${(product.grip[attribute] * 10) * 2}%`, backgroundColor: '#80d882' }}></div>
                            </div>
                        </div>)
                    })
                    } */}
                </div>
            </div>
        </div>
    )
}

export default ProductIntroMeta;


const getLabel = (label) => {
    let newlabel = `${label}`;
    switch (newlabel) {
        case "grip_snow_ice":
            newlabel = "Snow & Ice";
            break;
        case "grip_soft_muddy":
            newlabel = "Soft & Muddy";
            break;
        case "grip_hard_rocky":
            newlabel = "Hard & Rocky";
            break;
        case "grip_paths_trails":
            newlabel = "Paths & Trails";
            break;
        case "grip_road":
            newlabel = "Road";
            break;
        case "grip_versatility":
            newlabel = "Versatility";
            break;
        case "grip_lifting":
            newlabel = "Lifting";
            break;
        case "grip_cardio":
            newlabel = "Cardio";
            break;
        case "grip_natural":
            newlabel = "Natural";
            break;
    }
    return newlabel;
}


const currency = (value) => {
    let newValue = `${value}`;
    switch (newValue) {
        case "GBP":
            newValue = "£";
            break;
        case "USD":
            newValue = "$";
            break;
        case "EUR":
            newValue = "€";
            break;
    }
    return newValue;
} 
