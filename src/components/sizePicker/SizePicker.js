import React, { useEffect, useState } from 'react';

import './SizePicker.css';


const ProductSizes = ({ 
    product,
    gender,
    tableColor,
    onSelectSizeGuide,
    onSelectDontHaveSize,
    onSelectSizePicker
}) => {
 

    const [sizes, setSizes] = useState([]);
 /*    const [fromDataBase, setFromDataBase] = useState(false);
    const [notFromDataBase, setNotFromDataBase] = useState(false); */


    useEffect(() => {
        if(!gender){
            let index = product.configurable_options[0].attribute_code === "shoe_size"? 0 : 1;
            const pressed = product.configurable_options[index].values.map(size => {
                size.pressed = false;
                return size;
            })
            setSizes(pressed);

        } else if(gender === "male"){
            setSizes(product.sizes);
        } else if(gender === "female"){
            setSizes(product.sizes);
        }
    }, [product])
            

const clicker = (sizes, index) => {
    
    
    const newSizes = [...sizes]

    newSizes.map((size, i) => {
        if(i === index){
            newSizes[index].pressed = !newSizes[index].pressed
        } else{
            size.pressed = false
        }
    });
  
 
    setSizes(newSizes)
    onSelectSizePicker(product, gender)
}

const clicker2 = (sizes, index) => {
   
  /*   sizes.slice(index, 1);
   
    setSizes(sizes)
    onSelectSizePicker() */
}
    return (
        <>
            {!gender && <div className="sizePicker__links">
                <div className="sizePicker__links__right">
                    <p onClick={onSelectSizeGuide}><u>Size Guide</u></p>
                </div>
                <div className="sizePicker__links__left">
                    <p onClick={onSelectDontHaveSize}><u>Don't have your size?</u></p>
                </div>
            </div>}
            <div className="sizePicker__table" style={{borderColor: tableColor, color: tableColor}}>
                { 
                    !gender && sizes.map((sizeConfig, i) => {
                        return <div key={`size1${i}`}  onClick={() => clicker(sizes, i)} className={sizeConfig.pressed? `pressed`: 'no'} style={{borderColor: tableColor, color: tableColor}}>{sizeConfig.default_label}</div>;
                    })
                } 
                { 
                    gender && sizes.map((size, i) => {
                        return <div key={`size2${i}`} onClick={() => clicker2(sizes, i)} className={size.pressed? `pressed`: ''} style={{borderColor: tableColor, color: tableColor}}>{size}</div>;
                    })
                }
            </div>
        </>
    )
}

export default ProductSizes;
