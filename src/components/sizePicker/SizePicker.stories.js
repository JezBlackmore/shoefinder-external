import React from 'react';

import Grid from '../grid/Grid';

import SizePicker from './SizePicker';



const product = {
    "configurable_options": [
        {
          "id": 5469,
          "label": "Shoe Size (UK)",
          "product_id": 9810,
          "attribute_code": "shoe_size",
          "values": [
            {
              "value_index": 45,
              "label": "3",
              "store_label": "3",
              "default_label": "3",
              "use_default_value": true
            },
            {
              "value_index": 46,
              "label": "3.5",
              "store_label": "3.5",
              "default_label": "3.5",
              "use_default_value": true
            },
            {
              "value_index": 47,
              "label": "4",
              "store_label": "4",
              "default_label": "4",
              "use_default_value": true
            },
            {
              "value_index": 48,
              "label": "4.5",
              "store_label": "4.5",
              "default_label": "4.5",
              "use_default_value": true
            },
            {
              "value_index": 49,
              "label": "5",
              "store_label": "5",
              "default_label": "5",
              "use_default_value": true
            },
            {
              "value_index": 50,
              "label": "5.5",
              "store_label": "5.5",
              "default_label": "5.5",
              "use_default_value": true
            },
            {
              "value_index": 15,
              "label": "6",
              "store_label": "6",
              "default_label": "6",
              "use_default_value": true
            },
            {
              "value_index": 16,
              "label": "6.5",
              "store_label": "6.5",
              "default_label": "6.5",
              "use_default_value": true
            },
            {
              "value_index": 17,
              "label": "7",
              "store_label": "7",
              "default_label": "7",
              "use_default_value": true
            },
            {
              "value_index": 18,
              "label": "7.5",
              "store_label": "7.5",
              "default_label": "7.5",
              "use_default_value": true
            },
            {
              "value_index": 51,
              "label": "8",
              "store_label": "8",
              "default_label": "8",
              "use_default_value": true
            },
            {
              "value_index": 52,
              "label": "8.5",
              "store_label": "8.5",
              "default_label": "8.5",
              "use_default_value": true
            }
          ]
        },
        {
          "id": 6009,
          "label": "Colours",
          "product_id": 9810,
          "attribute_code": "colours",
          "values": [
            {
              "value_index": 620,
              "label": "Black/Black",
              "store_label": "Black/Black",
              "default_label": "Black/Black",
              "use_default_value": true
            },
            {
              "value_index": 799,
              "label": "Pink/Purple/Blue",
              "store_label": "Pink/Purple/Blue",
              "default_label": "Pink/Purple/Blue",
              "use_default_value": true
            }
          ]
        }
    ]
}

export default {
    title: 'Shoe Finder/Product Meta Combined/Individual/Size Picker',
    component: SizePicker,
    argTypes: {
        product: {control: 'object'},
        tableColor: {control: 'color'}
    }
}



const Template = (args) => <Grid columns={3}><SizePicker {...args} /></Grid>

export const sizePicker = Template.bind({})
sizePicker.args = {
    product: product,
    tableColor: 'black'
}
