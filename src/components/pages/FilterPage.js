
import React, { useState } from 'react';
/* import { useHistory } from 'react-router-dom'; */
/* import Button from '../button/Button'; */

import Context from '../../context/Context';

import buttonsJson from '../../buttons.json';
import buttonsJson2 from '../../buttons.json';

import Filter from '../filter/Filter';
import ProductCard from '../productCard/ProductCard';
import ProductMetaCombined from '../metaCombined/ProductMetaCombined';
import RelatedProducts from '../relatedProducts/RelatedProducts';
import ActivitySelector from '../activitySelector/ActivitySelector';
/* import products from '../../products.json'; */
import products from '../../Products2.json';
import SideNavLeft from '../sideNavLeft/SideNavLeft';
import { useQuery, gql, useMutation } from '@apollo/client';

const PRODUCTS = gql`
{
    category(id: 6) {
      
         name
      products{
        total_count
        page_info {
          current_page
          page_size
        }
        items{
          id
          sku
          stock_status
          name
          price {
            amount:regularPrice{
              amount{
                currency
                value
              }
            }
          }
        }
      }
    }
  }
`;

const EMPTYCARTID = gql`
mutation {
  createEmptyCart
}
`;

const ADDPRODUCTTOCART = gql`
mutation mutateFunctionAddProductToCart($text: String!) {
  addSimpleProductsToCart(
    input: {
      cart_id: $text
      cart_items: [
        {
          data: {
            quantity: 1
            sku: "5054167540867"
          }
        }
      ]
    }
  ) {
    cart {
      items {
        id
        product {
          sku
          stock_status
        }
        quantity
      }
    }
  }
}
`;

const FilterPage = () => {

  const { activeProduct,
    setActiveProduct,
    showSideNav,
    setShowSideNav,
    setShowList,
    setShowDontHavSize,
    setShowSizeGuide } = React.useContext(Context);


  const [runningProducts, setRunningProducts] = React.useState(products.filter(product => product.category === "Run"))
  const [gymProducts, setGymProducts] = React.useState(products.filter(product => product.category === "Train"))
  const [hikingProducts, setHikingProducts] = React.useState(products.filter(product => product.category === "Hike"));
  const [sortedProducts, setSortedProducts] = React.useState([])
  const [colorSelected, setColorSelected] = React.useState([])
  const [allProducts, setAllProducts] = React.useState(products);
  const [related, setRelated] = React.useState([])


  const [productsToDisplay, setProductsToDisplay] = React.useState(products);
  const [previousProductsToDisplay, setPreviousProductsToDisplay] = React.useState(products);
  const [activityType, setActivityType ] = React.useState('');
  const [chosenChoices, setChosenChoices] = React.useState(chosenChoicesObj);
  const [showSideNavLeft, setShowSideNavLeft] = React.useState(false);
  const [sideNavLeftData, setSideNavLeftData] = React.useState(null);
  const [showAddToBasket, setShowAddToBasket] = React.useState(false);
  const [disabledButtons, setDisabledButtons] = React.useState([]);
  const [memoryArrays, setMemoryArrays] = React.useState([]);
  const [memoryIndex, setMemoryIndex] = React.useState([]);
  const [thumbnailMainImage, setThumbnailMainImage] = React.useState([]);

  const [ buttonsOriginal, setButtonsOriginal ] = React.useState(JSON.parse(JSON.stringify(buttonsJson)));
  const [ActivitySelectorOptions, setActivitySelectorOptions] = React.useState(buttonsJson.map(button => button.type));

  const [selectedColor, setSelectedColor] = React.useState(null);
const [indexOfColourChosen, setIndexOfColourChosen] = React.useState(null);
const [cartId, setCartId] = useState('');

const [chosenGenderMorF, setChosenGenderMorF] = React.useState("male");
const [genderPickerFirstLoad, setGenderPickerFirstLoad] = React.useState(true);


const [mutateFunction, { dataAddToCart, loadingAddToCart, errorAddToCart }] = useMutation(EMPTYCARTID);
const [mutateFunctionAddProductToCart, { dataAddProductToCart, loadingAddProductToCart, errorAddProductToCart}] = useMutation(ADDPRODUCTTOCART);
const { loading, error, data } = useQuery(PRODUCTS);


const GraphQL = () => {   
    if (loading) return <p>Loading...</p>;
    if (error) {
      return <p>Error :(</p>;
    }
    return (
        data.category.products.items.map(product => <p>{product.name}</p>)
    )
}


/* const GraphQlEmptyCart = async () => {
  if (loadingAddToCart){
     console.log("Loading...");
     return;
  }
  if (errorAddToCart) {
   console.log(errorAddToCart);
   return;
  }
  const result = await dataAddToCart;

  console.log(333, result)
  return result;
} */

GraphQL(); 




  const onSelectProductCard = (product) => {
    window.scrollTo(0, 0)

 /*    console.log(3333333, product.image.url) */
    setThumbnailMainImage(product.image.url)
    setActiveProduct([ product ])

    if (activityType === "running") {
      const firstFour = runningProducts.slice(0 , 3)
      setRelated(firstFour)
    } else if (activityType === "hiking") {
      const firstFour = hikingProducts.slice(0 , 3)
      setRelated(firstFour)
    } else if (activityType === "train") {
      const firstFour = gymProducts.slice(0 , 3)
      setRelated(firstFour)
    } else{
      const firstFour = allProducts.slice(0 , 3)
      setRelated(firstFour)
    } ;


  }

  const onSelectSizeGuide = (event) => {
    setShowDontHavSize(false);
    setShowList(false);

    setShowSideNav(true);
    setShowSizeGuide(true);
  }

  const onSelectDontHaveSize = () => {
    setShowSizeGuide(false);
    setShowList(false);

    setShowSideNav(true);
    setShowDontHavSize(true);
  }

  const onSelectFindOutMore = (product) => {
    /*  const slug = product.name.replace(/'/g, '').split(" ").join("-"); */
    /*    window.location.href = `https://www.inov-8.com/${slug}`; */

   /*  console.log(product) */
    window.location.href = product[0].canonical_url;
  }

  const onSelectSizePicker = (product, gender) => {
    if(!gender){
      let index = product.configurable_options[0].attribute_code === "shoe_size"? 0 : 1;
      const checkIfSizeIsSectected = product.configurable_options[index].values.some(size => size.pressed)

      if(checkIfSizeIsSectected){
        setShowAddToBasket(true);
      } else {
        setShowAddToBasket(false);
      }
    }
    /*     } else if(gender === "male"){
           product.sizes.some(size => size.pressed);
        } else if(gender === "female"){
           product.sizes.some(size => size.pressed);
        } */


  }

  const onSelectRelatedProducts = (product) => {
    console.log(999991, product.image.url)
    setThumbnailMainImage(product.image.url)
    setGenderPickerFirstLoad(true);
    setChosenGenderMorF("male");
    setSelectedColor(null);

    window.scrollTo(0, 0)
   /*  console.log('click'); */
    if (activityType === "running") {
      const firstFour = runningProducts.slice(0 , 3)
      setRelated(firstFour)
    } else if (activityType === "hiking") {
      const firstFour = hikingProducts.slice(0 , 3)
      setRelated(firstFour)
    } else if (activityType === "train") {
      const firstFour = gymProducts.slice(0 , 3)
      setRelated(firstFour)
    } else{
      const firstFour = allProducts.slice(0 , 3)
      setRelated(firstFour)
    } ;
    setActiveProduct([ product ]);
  }

  const setSelectBackButton = () => {
    helperFunctionResetEverything()
  }

  const setSelectOnFilterReset = () => {
    helperFunctionResetEverything()
  }

  const helperFunctionResetEverything = () => {

    setChosenGenderMorF("male");
    setGenderPickerFirstLoad(true);
    setSelectedColor(null);

    
    setRelated([]);
    setMemoryIndex([]);
    setMemoryArrays([]);
    setButtonsOriginal(JSON.parse(JSON.stringify(buttonsJson2)));
    setActivityType('')
    setActiveProduct([]);
    setChosenChoices({});

    const buttonsToTrue = Object.keys(disabledButtons).map(key => disabledButtons[key] = true);
    setDisabledButtons(buttonsToTrue);
    setProductsToDisplay(products.map(product => {
      product.knockedOut = false
      return product;
    }));
  }

  const setSelectOnFilterButtonPress = (choiceObj, activity, pressed) => {

    setActiveProduct([]);
    setRelated([])

    const buttonsCopy = buttonsOriginal.map(i=>({...i}) )


    const toReplace = JSON.parse(JSON.stringify(activity))

    if(pressed === true){
      delete choiceObj[activity.value];
      /*    setProductsToDisplay(previousProductsToDisplay); */

      if (activityType === "running") {
        /*  const products = helperFunction(runningProducts); */
        /*      setProductsToDisplay(runningProducts) */
        filterProducts(choiceObj, buttonsCopy, activity, runningProducts, pressed);
      } else if (activityType === "train") {
        /* const products = helperFunction(gymProducts); */
        /*   setProductsToDisplay(gymProducts) */
        filterProducts(choiceObj, buttonsCopy, activity, gymProducts, pressed);
      } else if (activityType === "hiking") {
        /*   const products = helperFunction(hikingProducts); */
        /*    setProductsToDisplay(hikingProducts) */
        filterProducts(choiceObj, buttonsCopy, activity, hikingProducts, pressed);
      }

    } else{
      setChosenChoices(choiceObj);
      filterProducts(choiceObj, buttonsCopy, activity, productsToDisplay, pressed);
    }




  }




  const disableButtons = (filtered, activity, pressed) => {

    let btns = [
      {
        btn01: filtered.some(item=>item.runactivity_fells>0),
        btn02: filtered.some(item=>item.runactivity_fells>0),
        btn03: filtered.some(item=>item.runactivity_trails>0),
        btn04: filtered.some(item=>item.runactivity_ultra>0),
        btn05: filtered.some(item=>item.runactivity_ocr>0),
        btn06: filtered.some(item=>item.runactivity_orienteering>0),
        btn07: filtered.some(item=>item.runactivity_parkrun>0),
        btn08: filtered.some(item=>item.runactivity_road>0),
        btn09: filtered.some(item=>item.runactivity_swimrun>0)
      },
      {
        btn10: filtered.some(item=>item.runterrain_hardandrocky>0),
        btn11: filtered.some(item=>item.runterrain_softandmuddy>0),
        btn12: filtered.some(item=>item.runactivity_roads>0),
        btn13: filtered.some(item=>item.runactivity_pathsandtrails>0),
        btn14: filtered.some(item=>item.runactivity_iceandsnow>0),
        btn15: filtered.some(item=>item.runactivity_multiterrain>0),
      },
      {
        btn16: filtered.some(item=>item.runwidth_narrow>0),
        btn17: filtered.some(item=>item.runwidth_normal>0),
        btn18: filtered.some(item=>item.runwidth_wide>0),
      },
      {
        btn19: filtered.some(item=>item.runcushioing_soft>0),
        btn20: filtered.some(item=>item.runcushioing_moderate>0),
        btn21: filtered.some(item=>item.runcushioing_firm>0)
      },
      {
        btn22: filtered.some(item=>item.hiketype_boot>0),
        btn23: filtered.some(item=>item.hiketype_shoe>0)
      },
      {
        btn24: filtered.some(item=>item.hikewaterproof_yes>0),
        btn25: filtered.some(item=>item.hikewaterproof_no>0)
      },
      {
        btn26: filtered.some(item=>item.trainsession_workout>0),
        btn27: filtered.some(item=>item.trainsession_weightlifting>0),
        btn28: filtered.some(item=>item.trainsession_hiit>0),
        btn29: filtered.some(item=>item.trainsession_run>0)
      },
      {
        btn30: filtered.some(item=>item.trainsupport_minimal>0),
        btn31: filtered.some(item=>item.trainsupport_standard>0),
        btn32: filtered.some(item=>item.trainsupport_high>0),
        btn33: filtered.some(item=>item.trainsupport_veryhigh>0),
      },
      {
        btn34: filtered.some(item=>item.trainresponsiveness_low>0),
        btn35: filtered.some(item=>item.trainresponsiveness_standard>0),
        btn36: filtered.some(item=>item.trainresponsiveness_high>0)
      }
    ]


    const sameGroupToDisable = btns.map((arr, indexMain) => {

          if(memoryIndex.includes(indexMain) && !pressed) {

            let toReturnIndex = null;
            const arrKeys = Object.keys(arr);
            memoryArrays.forEach((mem, i) => {
              if(mem.hasOwnProperty(arrKeys[0])){
                toReturnIndex = i;
              }
            })

            return  memoryArrays[toReturnIndex]

          } else if(memoryIndex.includes(indexMain) && pressed) {
            let toReturnIndex = null;
            const arrKeys = Object.keys(arr);
            memoryArrays.forEach((mem, i) => {
              if(mem.hasOwnProperty(arrKeys[0])){
                toReturnIndex = i;
              }
            })


            const removedPressedRowArray = memoryArrays.splice(toReturnIndex, 0);
            const indexToRemove = memoryIndex.findIndex(item => item === indexMain);
            const removedPressedRowIndex = memoryIndex.splice(indexToRemove, 0)
            /*    console.log(memoryArrays)
               console.log(removedPressedRowArray) */
            setMemoryArrays(removedPressedRowArray)
            setMemoryIndex(removedPressedRowIndex)

            return arr;

          } else if(arr.hasOwnProperty(activity.identifier) && !memoryIndex.includes(indexMain)){

            Object.keys(arr).forEach((btn, i) => {

              if(btn !== activity.identifier){
                arr[btn] = false;

              } else if (btn !== activity.identifier){
                arr[btn] = true;
              }
            })

            let newMemory = [ ...memoryArrays, arr ];
            let memIndex = [ ...memoryIndex,  indexMain];

            setMemoryIndex(memIndex);
            setMemoryArrays(newMemory);


            return arr;
          }   else{
            return arr;
          }
        }
    )


    let group = []

    if (activityType === "running") {
      group = [0, 1, 2, 3];
    } else if (activityType === "hiking") {
      group = [4, 5]
    } else if (activityType === "train") {
      group = [6, 7, 8]
    };

    let count = 0;
    group.forEach( arr => {
      if(memoryIndex.includes(arr)){
        count++
      }
    })


    if(count === group.length -1 && count !== 0){
      setActiveProduct([sortedProducts[0]])
      setRelated(allProducts)
    }


    const combineGroupsIntoOne = sameGroupToDisable.reduce((acc, cur) => {
      return { ...acc, ...cur}
    }, {})


    setDisabledButtons(combineGroupsIntoOne);
  }



  const filterProducts = (choiceObj, buttonsCopy, activity, products, pressed) => {

    setPreviousProductsToDisplay(productsToDisplay);
    /*   const allProducts = getAllProducts(); */

    products.forEach(product => {
      product.knockedOut = false;
    })


    const keys = Object.keys(choiceObj);

    const productsThatFit = products.filter(product => {
      const trueOrFalse = [];

      keys.forEach(key => {
        if( product[key] > 1){
          trueOrFalse.push(true)
        } else{
          trueOrFalse.push(false)
        }
      })
      return trueOrFalse.every(boolean => boolean);
    })

    const addATotal =  productsThatFit.map(product => {

      let total = 0;

      keys.forEach(key => {
        total += product[key]
      })

      product.total += total;
      return product;
    })

    const sorted =  addATotal.sort((a, b) => {
      return a.total - b.total;
    });

    let knockedOutProducts;

    if (activityType === "running") {
      knockedOutProducts = runningProducts.filter(function(val) {
        return sorted.indexOf(val) === -1;
      });
    } else if (activityType === "train") {
      knockedOutProducts = gymProducts.filter(function(val) {
        return sorted.indexOf(val) === -1;
      });
    } else if (activityType === "hiking") {
      knockedOutProducts = hikingProducts.filter(function(val) {
        return sorted.indexOf(val) === -1;
      });
    }


    const addFade = [...knockedOutProducts].map(product => {
      product.knockedOut = true;
      return product;
    });

    setSortedProducts(sorted)
    if(sorted.length === 1){
      window.scrollTo(0, 0)
      setActiveProduct([ sorted[0] ])
      setRelated(allProducts)
    } else if(productsThatFit.length < 1){
      window.scrollTo(0, 0)
      setActiveProduct([products[0]])
      setRelated(allProducts)

    } /* else  if(sorted.length > 1 && all filters clicked){

                } */ else{
      setProductsToDisplay([...sorted , ...addFade])
      disableButtons(sorted, activity, pressed)
    }
  }


  React.useEffect(() => {

    allProducts.map(product => {
      product.displayName = product.name.replace(/women's/gmi, '').replace(/men's/gmi, '').trim();
      return product;
    });


    setAllProducts(allProducts)

    setProductsToDisplay([...allProducts]);

  }, [])



  const setSelectOnActivityTypeButton = (activityType) => {

    setActiveProduct([]);
    setActivityType(activityType);
    if (activityType === "running") {
      /*  const products = helperFunction(runningProducts); */
      setProductsToDisplay(runningProducts)
    } else if (activityType === "train") {
      /* const products = helperFunction(gymProducts); */
      setProductsToDisplay(gymProducts)
    } else if (activityType === "hiking") {
      /*   const products = helperFunction(hikingProducts); */
      setProductsToDisplay(hikingProducts)
    }
  }



  const onSelectGenderPicker = (product, indexOfColour, valuesIndex, gender, thumbnail) => {
    setThumbnailMainImage(thumbnail)

    setChosenGenderMorF(gender);
    setGenderPickerFirstLoad(false);
    setSelectedColor(valuesIndex);

  }

  const setSelectOnInformationButton = (option) => {
    setShowSideNavLeft(true);

    setSideNavLeftData([{
      mainTitle : option.labelInformation.title,
      data: option.labelInformation.data
    }])
  }

  const onSelectAddToBasket = async (product) => {

    if (loadingAddToCart){
      console.log("Loading...");
      return;
   }
   if (errorAddToCart) {
    console.log(errorAddToCart);
    return;
   }

   const data = await mutateFunction();
   setCartId(data.data.createEmptyCart);
  console.log(data.data.createEmptyCart);
  const CARD_ID = `${data.data.createEmptyCart}`;
  
  if (loadingAddProductToCart){
    console.log("Loading...");
    return;
 }
 if (errorAddProductToCart) {
  console.log(errorAddProductToCart);
  return;
 }
  const dataAddProductToCart = mutateFunctionAddProductToCart({ variables: { text: CARD_ID } });
console.log(dataAddProductToCart);
  }

  const backToOverviewButton = () => {

  /*   console.log(909009090909, "NOW NOW NOW ") */
 setChosenGenderMorF("male");
    setGenderPickerFirstLoad(true);
    setSelectedColor(null);


    helperFunctionResetEverything();
  }

  return (
    <div className={activeProduct.length < 1 ? "mainContainer": "mainContainerWhenProductActive"}>
     
      <div className="filterContainer">

     {!activityType && <ActivitySelector 
          setSelectOnActivityTypeButton={setSelectOnActivityTypeButton}
          ActivitySelectorOptions={ActivitySelectorOptions} 
          activeProduct={activeProduct}
          setActiveProduct={setActiveProduct}
          backToOverviewButton={backToOverviewButton}/> 
          }

          {showSideNavLeft && sideNavLeftData && <SideNavLeft
              showSideNavLeft={showSideNavLeft}
              setShowSideNavLeft={setShowSideNavLeft}
              topAndBottomPadding={40}
              sidePadding={40} >
            {sideNavLeftData.map((obj, i) => {
              return (
                  <div key={`div${i}`}>
                    {obj.mainTitle && <h2>{obj.mainTitle}</h2>}
                    { obj.data.map( (data, i) => {
                      return (
                          <div>
                            {data.subtitle && <h3>{data.subtitle}</h3>}
                            {data.description && <p>{data.description}</p>}
                          </div>
                      )
                    })}
                  </div>
              )
            })}
          </SideNavLeft>
          }


          {activityType &&  <Filter
              disabledButtons={disabledButtons}
              buttons={buttonsOriginal}
              type={activityType}
              chosenChoices={chosenChoices}
              setChosenChoices={setChosenChoices}
              setSelectBackButton={setSelectBackButton}
              setSelectOnFilterReset={setSelectOnFilterReset}
              setSelectOnFilterButtonPress={setSelectOnFilterButtonPress}
              setSelectOnInformationButton={setSelectOnInformationButton}
          />
          }
        </div>
        { activeProduct.length < 1 ? <div className="productContainer">
          <div className="productContainer__cards">
            {productsToDisplay.map((item, i) => {
              return (
                  <>
                    <ProductCard product={item} onSelectProductCard={onSelectProductCard} key={i}/>

                  </>

              )
            })}
          </div>
        </div> : <><div className="productAttributesContainer">
          <div className="activeProductHeader">
            <h2>WE RECOMMEND</h2>
            {/*   <Button   style={'secondary'} text={"Back To Overview"} onSelectButton={backToOverviewButton}/> */}
          </div>
          <ProductMetaCombined
              product={activeProduct}
              onSelectSizeGuide={onSelectSizeGuide}
              onSelectDontHaveSize={onSelectDontHaveSize}
              onSelectFindOutMore={onSelectFindOutMore}
              onSelectSizePicker={onSelectSizePicker}
              onSelectFindOutMore={onSelectFindOutMore}
              genderOptions={genderOptions}
              onSelectAddToBasket={onSelectAddToBasket}
              showAddToBasket={showAddToBasket}
              onSelectGenderPicker={onSelectGenderPicker}
              selectedColor={selectedColor}
              setSelectedColor={setSelectedColor} 
              chosenGenderMorF={chosenGenderMorF}
              genderPickerFirstLoad={genderPickerFirstLoad}
              thumbnailMainImage={thumbnailMainImage}
              setThumbnailMainImage={setThumbnailMainImage}
          />

          <RelatedProducts related={related} onSelectRelatedProducts={onSelectRelatedProducts} />
        </div>


          <div className="productAttributesContainer__mobile">
            <div className="productAttributesContainer__mobile__back">
              {/*   <Button   style={'secondary'} text={"Back To Overview"} onSelectButton={backToOverviewButton}/> */}
              <p onClick={() => backToOverviewButton()}><i className="fa fa-long-arrow-left" aria-hidden="true"></i> Back A Step</p>
            </div>
            <div className="activeProductHeader">
              <h2>WE RECOMMEND</h2>
            </div>
            <ProductMetaCombined
                product={activeProduct}
                onSelectSizeGuide={onSelectSizeGuide}
                onSelectDontHaveSize={onSelectDontHaveSize}
                onSelectFindOutMore={onSelectFindOutMore}
                onSelectSizePicker={onSelectSizePicker}
                onSelectFindOutMore={onSelectFindOutMore}
                genderOptions={genderOptions}
                onSelectAddToBasket={onSelectAddToBasket}
                showAddToBasket={showAddToBasket}
                onSelectGenderPicker={onSelectGenderPicker}
                setSelectedColor={setSelectedColor}
                selectedColor={selectedColor}
                chosenGenderMorF={chosenGenderMorF}
                genderPickerFirstLoad={genderPickerFirstLoad}
                thumbnailMainImage={thumbnailMainImage}
               
                />
            <RelatedProducts 
              related={related}
              onSelectRelatedProducts={onSelectRelatedProducts} 
              setThumbnailMainImage={setThumbnailMainImage}/>
          </div> </>}

      </div>
  )
}

export default FilterPage;

const chosenChoicesObj = {};


const genderOptions = [
  {
    "name": "All Terrain Pro Mitt",
    "regularPrice": {
      "adjustments": [],
      "amount": {
        "value": 140,
        "currency": "GBP"
      }
    },
    "special_price": null,
    "thumbnail": {
      "label": "inov-8 All Terrain Pro Mitt - sideview",
      "url": "https://www.inov-8.com/media/catalog/product/cache/bd1b8cad7c61ae1ddc34981e467e2b22/a/l/all_terrain_pro_mitt.jpg"
    }
  },
  {
    "name": "All Terrain Pro Mitt",
    "regularPrice": {
      "adjustments": [],
      "amount": {
        "value": 140,
        "currency": "GBP"
      }
    },
    "special_price": null,
    "thumbnail": {
      "label": "inov-8 All Terrain Pro Mitt - sideview",
      "url": "https://www.inov-8.com/media/catalog/product/cache/bd1b8cad7c61ae1ddc34981e467e2b22/a/l/all_terrain_pro_mitt.jpg"
    }
  }
]

/* 
const related = [{
  "name": "All Terrain Pro Mitt",
  "regularPrice": {
    "adjustments": [],
    "amount": {
      "value": 140,
      "currency": "GBP"
    }
  },
  "special_price": null,
  "thumbnail": {
    "label": "inov-8 All Terrain Pro Mitt - sideview",
    "url": "https://www.inov-8.com/media/catalog/product/cache/bd1b8cad7c61ae1ddc34981e467e2b22/a/l/all_terrain_pro_mitt.jpg"
  }
},
{
  "name": "All Terrain Pro Mitt",
  "regularPrice": {
    "adjustments": [],
    "amount": {
      "value": 140,
      "currency": "GBP"
    }
  },
  "special_price": null,
  "thumbnail": {
    "label": "inov-8 All Terrain Pro Mitt - sideview",
    "url": "https://www.inov-8.com/media/catalog/product/cache/bd1b8cad7c61ae1ddc34981e467e2b22/a/l/all_terrain_pro_mitt.jpg"
  }
},
{
  "name": "All Terrain Pro Mitt",
  "regularPrice": {
    "adjustments": [],
    "amount": {
      "value": 140,
      "currency": "GBP"
    }
  },
  "special_price": null,
  "thumbnail": {
    "label": "inov-8 All Terrain Pro Mitt - sideview",
    "url": "https://www.inov-8.com/media/catalog/product/cache/bd1b8cad7c61ae1ddc34981e467e2b22/a/l/all_terrain_pro_mitt.jpg"
  }
}
] */