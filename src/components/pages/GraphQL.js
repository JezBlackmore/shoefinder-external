import React from 'react';
import { useQuery, gql } from '@apollo/client';


const PRODUCTS = gql`
{
    category(id: 6) {
     
         name
      products{
        total_count
        page_info {
          current_page
          page_size
        }
        items{
          id
          sku
          stock_status
          name
          price {
            amount:regularPrice{
              amount{
                currency
                value
              }
            }
          }
        }
      }
    }
  }
`;

const GraphQL = () => {
console.log(useQuery(PRODUCTS))
    const { loading, error, data } = useQuery(PRODUCTS);

    if (loading) return <p>Loading...</p>;
   
    if (error) {
      console.log(4444444, error)
      return <p>Error :(</p>;
    }

    return (
        data.category.products.items.map(product => <p>{product.name}</p>)
    )
}

export default GraphQL;