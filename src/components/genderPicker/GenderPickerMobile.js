import React, { useEffect, useState } from 'react';

import './GenderPickerMobile.css';
import GenderPickerMobileCard from './GenderPickerMobileCard';


const GenderPicker = ({ 
    product,
    onSelectGenderPicker,
    setSelectedColor,
    selectedColor,
    chosenGenderMorF,
    arrayIndex,
    genderPickerFirstLoad}) => {

    const [indexToUse, setIndexToUse ]= useState(null);

    const [show, setShow ]= useState(false);
    const [selectedColourIndexState, setSelectedColourIndexState] = useState(null)

    useEffect(() => {
 
        
        const index = product[0].configurable_options[0].attribute_code === "colours" ? 0 : 1;

       /*  console.log(1111111111, product[0].configurable_options[0].attribute_code)
        console.log(99999, index)
 */
      /*   console.log(3333333333, product[0].configurable_options[index].values.map( color => <p>{color.store_label}</p>)) */
        setIndexToUse(index)
        setShow(true)
          
    }, [product])


const [showMens, setShowMens] = useState(true)
    
    return (
        show && <>

            <div className="genderPickerMobile__tabs">
                    <div onClick={() => setShowMens(true)} className={`genderPickerMobile__tabs__header ${showMens ? `activeTab`: ''}`}> <h4>Men's</h4> </div>
                    <div onClick={() => setShowMens(false)} className={`genderPickerMobile__tabs__header ${!showMens ? `activeTab`: ''}`}> <h4>Women's</h4> </div>
            </div>
            <div className="genderPickerMobile__tabs__results">
            {showMens && <div className="genderPickerMobile__mens">
               
                <div className="genderPickerMobile__mens__colours">
                {product[0].configurable_options[indexToUse].values.map(  (colour, index) => {

                 

                    return < GenderPickerMobileCard 
                    key={`colorMobileM${index}`} 
                    currentlyPressed={colour.pressed} 
                    selectedColourIndex={colour.value_index}
                    indexOfColour={indexToUse} 
                    valuesIndex={index}  
                    product={product} 
                    onSelectGenderPicker={onSelectGenderPicker}
                    setSelectedColor={setSelectedColor}
                    selectedColor={selectedColor}
                    gender={"male"}
                    chosenGenderMorF={chosenGenderMorF}
                    arrayIndex={index}
                    genderPickerFirstLoad={genderPickerFirstLoad}
                    />
                })}
                </div>

               {/*  <img src={genderOptions[0].thumbnail.url} alt={genderOptions[0].thumbnail.label}/> */}
            </div>}
            {!showMens && <div className="genderPickerMobile__womens">
                <div className="genderPickerMobile__mens__colours">
                {product[0].configurable_options[indexToUse].values.map( (colour, index) => {

                 

                    return < GenderPickerMobileCard 
                    key={`colorMobileF${index}`} 
                    currentlyPressed={colour.pressed} 
                    selectedColourIndex={colour.value_index}
                    indexOfColour={indexToUse} 
                    valuesIndex={index} 
                    product={product} 
                    onSelectGenderPicker={onSelectGenderPicker}
                    setSelectedColor={setSelectedColor}
                    selectedColor={selectedColor}
                    gender={"female"}
                    chosenGenderMorF={chosenGenderMorF}
                    arrayIndex={index}
                    genderPickerFirstLoad={genderPickerFirstLoad}
                    />
                })}
                </div>
                
            </div>}
            </div>
        </>
    )
}

export default GenderPicker;