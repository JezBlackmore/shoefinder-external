
import React from 'react';
import GenderPicker from './GenderPicker';


const genderOptions = [
    {
        "name": "All Terrain Pro Mitt",
        "regularPrice": {
            "adjustments": [],
            "amount": {
              "value": 140,
              "currency": "GBP"
            }
        },
        "special_price": null,
        "thumbnail": {
            "label": "inov-8 All Terrain Pro Mitt - sideview",
            "url": "https://www.inov-8.com/media/catalog/product/cache/bd1b8cad7c61ae1ddc34981e467e2b22/a/l/all_terrain_pro_mitt.jpg"
        }
    },
    {
        "name": "All Terrain Pro Mitt",
        "regularPrice": {
            "adjustments": [],
            "amount": {
              "value": 140,
              "currency": "GBP"
            }
        },
        "special_price": null,
        "thumbnail": {
            "label": "inov-8 All Terrain Pro Mitt - sideview",
            "url": "https://www.inov-8.com/media/catalog/product/cache/bd1b8cad7c61ae1ddc34981e467e2b22/a/l/all_terrain_pro_mitt.jpg"
        }
    }
]

export default {
    title: 'Shoe Finder/GenderPicker',
    component: GenderPicker,
    argTypes: {
        genderOptions: { control: 'object' }
    }
}

const Template = (args) => <GenderPicker {...args} />

export const genderPicker = Template.bind({})
genderPicker.args = {
    genderOptions: genderOptions
}