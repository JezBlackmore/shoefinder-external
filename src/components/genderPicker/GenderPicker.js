import React, { useEffect, useState } from 'react';

import './GenderPicker.css';
import GenderPickerMobileCard from './GenderPickerMobileCard';

const GenderPicker = ({ 
    product, 
    onSelectGenderPicker, 
    setSelectedColor, 
    selectedColor, 
    chosenGenderMorF,
    genderPickerFirstLoad
}) => {

    const [indexToUse, setIndexToUse ]= useState(null);

    const [show, setShow ]= useState(false);


    useEffect(() => {
 
        
        const index = product[0].configurable_options[0].attribute_code === "colours" ? 0 : 1;
        setIndexToUse(index)
        setShow(true)
          
    }, [product])



    /* const {colourName, setColourName} = useState(product[0].configurable_options[indexToUse].values[valuesIndex].default_label);
const [ colourValueIndexProduct, setColourValueIndexProduct ] = useState(product[0].configurable_options[indexToUse].values[valuesIndex].value_index);
const [ colourValueIndexLookUp, setColourValueIndexLookUp ] = useState(product[0].configurable_options[indexToUse].values[valuesIndex].value_index);
const [ thumbNailImage, setThumbNailImage ] = useState(product[0].configurable_options[indexToUse].values[valuesIndex].value_index);
 */
   /*  const getColourImage = () => { */
        /* console.log(33333, product) */
        /* let thumbNailUrl = '';
            product[0].variants.forEach(variant => {
                if(variant.attributes[1].value_index === colourValueIndexProduct){
                    thumbNailUrl = variant.product.thumbnail.url;
                }
            })
        
            console.log(999, thumbNailUrl);
            return thumbNailUrl;
        }
        
        useEffect(() => {
            const getColourIndex = getColourImage();
            
            
            
            setColourValueIndexLookUp(getColourIndex); */
            /*setThumbNailImage(getColourIndex)
            console.log(8888, getColourIndex) */
       /*  }, [])  */
    
    return (
        show && <>
            <div className="genderPicker__mens">
                <p>Mens</p>
                <div className="genderPicker__mens__colours">
                {product[0].configurable_options[indexToUse].values.map((colour, index) => {

                   
                    
                     return < GenderPickerMobileCard 
                     product={product} 
                     onSelectGenderPicker={onSelectGenderPicker}
                     indexOfColour={indexToUse}
                     valuesIndex={index}
                     currentlyPressed={colour.pressed}
                     selectedColourIndex={colour.value_index}
                     key={`colorM${index}`}
                     setSelectedColor={setSelectedColor}
                     selectedColor={selectedColor}
                     gender="male"
                     chosenGenderMorF={chosenGenderMorF}
                     arrayIndex={index}
                     genderPickerFirstLoad={genderPickerFirstLoad}
                    />
                })}
                </div>

               {/*  <img src={genderOptions[0].thumbnail.url} alt={genderOptions[0].thumbnail.label}/> */}
            </div>
            <div className="genderPicker__womens">
                <p>Womens</p>
                <div className="genderPicker__mens__colours">
                {product[0].configurable_options[indexToUse].values.map((colour, index) => {
                    return < GenderPickerMobileCard 
                    product={product} 
                    onSelectGenderPicker={onSelectGenderPicker}
                    indexOfColour={indexToUse}
                    valuesIndex={index}
                    selectedColourIndex={colour.value_index}
                    currentlyPressed={colour.pressed}
                    key={`colorF${index}`}
                    colourChosen={colour}
                    setSelectedColor={setSelectedColor}
                    selectedColor={selectedColor}
                    gender="female"
                    chosenGenderMorF={chosenGenderMorF}
                    genderPickerFirstLoad={genderPickerFirstLoad}
                />

                })}
                </div>
                
            </div>
        </>
    )
}

export default GenderPicker;