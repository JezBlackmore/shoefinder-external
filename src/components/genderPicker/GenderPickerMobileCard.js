import React, { useState, useEffect } from 'react';

const GenderPickerCard = ({ 
    product, 
    onSelectGenderPicker,  
    indexOfColour,
    valuesIndex,
    currentlyPressed,
    colourChosen,
    setSelectedColor,
    selectedColor,
    gender,
    chosenGenderMorF,
    arrayIndex,
    genderPickerFirstLoad,
    selectedColourIndex}) => {


        const [thumbnailImage, setThumbnailImage ] = useState('');


       /*   console.log(product) */
   /*  const [active, setActive] = useState(currentlyPressed.pressed); */

/* const {colourName, setColourName} = useState(product[0].configurable_options[indexOfColour].values[valuesIndex].default_label);
const [ colourValueIndexProduct, setColourValueIndexProduct ] = useState(product[0].configurable_options[indexOfColour].values[valuesIndex].value_index);
const [ colourValueIndexLookUp, setColourValueIndexLookUp ] = useState(product[0].configurable_options[indexOfColour].values[valuesIndex].value_index);
const [ thumbNailImage, setThumbNailImage ] = useState(product[0].configurable_options[indexOfColour].values[valuesIndex].value_index); */
/* 
const getColourImage = () => {

let thumbNailUrl = '';
    product[0].variants.forEach(variant => {
        if(variant.attributes[1].value_index === colourValueIndexProduct){
            thumbNailUrl = variant.product.thumbnail.url;
        }
    })

    console.log(999, thumbNailUrl);
    return thumbNailUrl;
}

useEffect(() => {
    const getColourIndex = getColourImage();
    setColourValueIndexLookUp(getColourIndex);
    setThumbNailImage(getColourIndex)
    console.log(8888, getColourIndex)
}, [])  */


/* const [selectedColourIndexState, setSelectedColourIndexState] = useState(selectedColourIndex) */


    const colourPickerClicked = () => {

/* console.log(22222, product[0].variants[0].product.thumbnail.url) */



        setSelectedColor(product[0].configurable_options[indexOfColour].values[valuesIndex].default_label)
    
        onSelectGenderPicker(product[0], indexOfColour, product[0].configurable_options[indexOfColour].values[valuesIndex].default_label, gender,  thumbnailImage)

      /*   console.log(333, product[0].configurable_options[indexOfColour].values[valuesIndex].default_label) */
       /*  onSelectGenderPicker(product, indexOfColour, valuesIndex) */
    }

  useEffect(() => {

    console.log(222, selectedColourIndex)
 
let thumbnail;

/* let indexNum = 0; */
/* Object.entries() */
console.log(33333444444, Object.entries(product[0]))
if(Object.keys(product[0]).includes('variants')){
for (let i = 0; i < product[0].variants.length; i++){
    if(product[0].variants[i].attributes[1].value_index === selectedColourIndex){
        thumbnail = product[0].variants[i].product.thumbnail.url;
        break;
    } 

}
    setThumbnailImage(thumbnail)
    console.log(111, thumbnail);
} else{
    thumbnail = `https://via.placeholder.com/150`;
}

setThumbnailImage(thumbnail)

    }, [selectedColourIndex]) 
   
    const chooseWhichGenderToAddClass = () => {
        if(product[0].configurable_options[indexOfColour].values[valuesIndex].default_label === selectedColor && chosenGenderMorF == gender){
            return `chosenSquare`;
        } else if(arrayIndex === 0 && chosenGenderMorF == "male" && genderPickerFirstLoad === true){
            return `chosenSquare`;
        } else {
            return ``;
        }
    }


  /*   if(product[0].variants){ */
        /* console.log(product[0].variants.forEach(product => {
           
            console.log(product.product.color) */
           /*  console.log(product[1].product) */
     /*    }));
    }else{
        console.log("Not this one");
    } */
    


 /*    useEffect(() => {
        product[0].variants[0].product.thumbnail.url
       console.log(selectedColourIndexState)
          
    }, [selectedColourIndexState])
 */


    return(
       
        <div className={`genderPicker__mens__colours__image ${ chooseWhichGenderToAddClass() }`} onClick={() => colourPickerClicked()}>
            <img src={thumbnailImage/* product[0].thumbnail.url} alt={product[0].thumbnail.label */} /* onClick={() => colourPickerClicked()} *//>
        </div>
    )
}

export default GenderPickerCard;