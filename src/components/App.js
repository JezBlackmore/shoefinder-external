import React from 'react';
import {Switch, Route} from 'react-router-dom';
import Context from '../context/Context';
import SideNav from './sideNav/SideNav';
import DontHaveYourSizeForm from './dontHaveYourSizeForm/DontHaveYourSizeForm';
import Table from './table/Table';
import FilterPage from './pages/FilterPage';

import GraphQL from './pages/GraphQL';


const App = () => {
    const {
        showSideNav,
            setShowSideNav,
            showList,
            showDontHavSize,
            showSizeGuide,
    } = React.useContext(Context);




const renderAuthButton = () => {
    if(showSideNav && showDontHavSize){ 
       return <>
            <h3>Don't have your size?</h3>
            <p>Select your size below and we’ll email you when it’s back in stock</p>
            <DontHaveYourSizeForm
                textColour={'black'}
                acceptBGcolor={'white'}
                acceptFontColor={'black'}
                gender={'male'}
                tableColor={'white'}
                linkColor={'white'}
            />
        </>
    }
    if(showSideNav && showSizeGuide ){ 
       return  <>
            <h3>Size Guide</h3>
            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
            <Table
                cells={cells}
                tableHeaderFontSize={10}
                tableBodyFontSize={10}
                tableHeaderBackGroundColor={"white"}
                defaultColour={"white"}
                textAlign={"center"}
                tableHeaderFontColor={"black"}
            />
        </>
    }
    if(showSideNav && showList ){ 
       return  <>
            <h3>SOFT</h3>
            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
            <h3>MODERATE</h3>
            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
            <h3>FIRM</h3>
            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
        </>
        }
}
    
    return (
        <>
            <Switch>
                    <Route path="/shoefinder" exact component={FilterPage} />
                      <Route path="/graphql" component={GraphQL} /> 
                </Switch>
            <SideNav
                showSideNav={showSideNav}
                setShowSideNav={setShowSideNav}
                sidePadding={40}
                topAndBottomPadding={40}
            >
               {renderAuthButton()}
            </SideNav>
        </>
    )
}

export default App;

const cells = {
    lines: [
        ['UK size', 'EU size', 'US mens', 'US womens', 'Sizes in CM (Approx.)'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
        ['1', '33', '2', '2.5', '2'],
    ]
}